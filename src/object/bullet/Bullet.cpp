#include "object/bullet/Bullet.hpp"
#include <iostream>

namespace object
{
    static protocol::network::packet::GameObjectType getObjType() {
        return (protocol::network::packet::GameObjectType::MISSILE);
    }

    Bullet::Bullet(std::string id) : AObject(id, protocol::network::packet::GameObjectType::MISSILE)
    {
        _params.speed = 30;
        _size.first = 100;
        _size.second = 50;
        _score = 0;
        _lp = 1;
    }

    void Bullet::setSubType(const protocol::network::packet::GameObjectType &type)
    {
        if (type == protocol::network::packet::GameObjectType::PLAYER_MISSILE)
            _nextAction = protocol::network::packet::GameActionType::RIGHT;
        if (type == protocol::network::packet::GameObjectType::ENEMY_MISSILE)
            _nextAction = protocol::network::packet::GameActionType::LEFT;

        _subType = type;
    }

    protocol::network::packet::GameActionType Bullet::getNextAction()
    {
        return (_nextAction);
    }
}

extern "C"
{
    std::shared_ptr<object::IObject>    init_libObj(std::string id)
    {
        return (std::make_shared<object::Bullet>(id));
    }

    protocol::network::packet::GameObjectType getType()
    {
        return object::getObjType();
    }
}