#include "object/AObject.hpp"
#include <iostream>

namespace object
{
    AObject::AObject(std::string id, protocol::network::packet::GameObjectType type)
            : _id(id), _type(type), _lp(0), _power(0), _score(0), _size({0, 0})
    {
        _pos.x = 0;
        _pos.y = 0;
        _pos.z = 0;
        _state = protocol::network::packet::GameObjectState::ALIVE;
        _params.animated = true;
        _params.hasColl = false;
        _params.speed = 1;
        _indexActionList = 0;
    }

    AObject::AObject(std::string id, protocol::network::packet::GameObjectType type, std::shared_ptr<object::IObject> parent)
            : _id(id), _lp(0), _parentObject(parent), _power(0), _score(0), _size({0, 0}), _type(type)
    {
        _pos.x = 0;
        _pos.y = 0;
        _pos.z = 0;
        _indexActionList = 0;
    }

    std::string AObject::getId() const
    {
        return (_id);
    }

    std::pair<std::size_t, std::size_t> AObject::getSize() const
    {
        return (_size);
    }

    objectPos AObject::getPos() const
    {
        return (_pos);
    }

    std::string AObject::getAssetPath() const
    {
        return (_assetPath);
    }

    objectParams AObject::getParams() const
    {
        return (_params);
    }

    std::size_t AObject::getLp() const
    {
        return (_lp);
    }

    std::size_t AObject::getScore() const
    {
        return (_score);
    }

    std::size_t AObject::getPower() const
    {
        return (_power);
    }

    protocol::network::packet::GameObjectState AObject::getState() const
    {
        return (_state);
    }

    bool AObject::hasColl() const
    {
        return (_params.hasColl);
    }

    bool AObject::isAlive() const
    {
        return (_state == protocol::network::packet::GameObjectState::ALIVE);
    }

    bool AObject::isAnimated() const
    {
        return (_params.animated);
    }

    protocol::network::packet::GameObjectType AObject::getType() const
    {
        return (_type);
    }

    protocol::network::packet::GameObjectType AObject::getSubType() const
    {
        return (_subType);
    }

    std::shared_ptr<IObject> AObject::getParentObj()
    {
        return (_parentObject);
    }

    void AObject::setPos(const objectPos &newPos)
    {
        _pos = newPos;
    }

    void AObject::setSize(const std::pair<std::size_t, std::size_t> &newSize)
    {
        _size.first = newSize.first;
        _size.second = newSize.second;
    }

    void AObject::setAssetPath(const std::string &newPath)
    {
        _assetPath = newPath;
    }

    void AObject::setLp(const std::size_t &newLp)
    {
        _lp = newLp;
    }

    void AObject::setScore(const std::size_t &newScore)
    {
        _score = newScore;
    }

    void AObject::setPower(const std::size_t &newPower)
    {
        _power = newPower;
    }

    void AObject::setHasColl(const bool &newVal)
    {
        _params.hasColl = newVal;
    }

    void AObject::setOrientation(const objectOrientation &orientation)
    {
        _params.orientation = orientation;
    }

    void AObject::setSubType(const protocol::network::packet::GameObjectType &subType)
    {
        _subType = subType;
    }

    protocol::network::packet::GameActionType AObject::getNextAction()
    {
        return (_actionList.at(_indexActionList));
    }

    void AObject::setParentObj(const std::shared_ptr<IObject> &parent)
    {
        _parentObject = parent;
    }

    void AObject::setNextAction(const protocol::network::packet::GameActionType &nextAction)
    {
        _actionList.push_back(nextAction);
    }

    void AObject::nextAction()
    {
        _indexActionList = (_indexActionList + 1 < _actionList.size()) ? ++_indexActionList : 0;
    }

    void AObject::setState(const protocol::network::packet::GameObjectState &newState)
    {
        _state = newState;
    }
}