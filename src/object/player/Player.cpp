#include "object/player/Player.hpp"
#include <iostream>

namespace object
{
    static protocol::network::packet::GameObjectType getObjType() {
         return (protocol::network::packet::GameObjectType::PLAYER);
    }

    Player::Player(std::string id) : AObject(id, protocol::network::packet::GameObjectType::PLAYER) {
        _params.speed = 40;
        _size.first = 230;
        _size.second = 150;
        _score = 9877;
        _lp = 76;
        _params.animated = false;
        _params.orientation = objectOrientation::EST;
        _nextAction = protocol::network::packet::GameActionType::UNDEFINED;
    }

    protocol::network::packet::GameActionType Player::getNextAction()
    {
        return (_nextAction);
    }

    void Player::setNextAction(const protocol::network::packet::GameActionType &newAction)
    {
        _nextAction = newAction;
    }

    void Player::nextAction()
    {
        _nextAction = protocol::network::packet::GameActionType::UNDEFINED;
    }
}

extern "C"
{
    std::shared_ptr<object::IObject>    init_libObj(std::string id)
    {
        return (std::make_shared<object::Player>(id));
    }

    protocol::network::packet::GameObjectType getType()
    {
        return object::getObjType();
    }
}