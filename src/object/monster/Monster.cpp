#include "object/monster/Monster.hpp"
#include <iostream>

namespace object
{
    static protocol::network::packet::GameObjectType getObjType() {
        return (protocol::network::packet::GameObjectType::MONSTER);
    }

    Monster::Monster(std::string id) : AObject(id, protocol::network::packet::GameObjectType::MONSTER)
    {
        _params.speed = 20;
        _pos.x = 1500;
        _pos.y = 100;
        _size.first = 250;
        _size.second = 150;
        _score = 0;
        _lp = 1;
        this->initListActions();
        this->setSubType(protocol::network::packet::GameObjectType::MONSTER_LEVEL1);
    }

    void Monster::initListActions()
    {
        _actionList.push_back(protocol::network::packet::GameActionType::LEFT);
        _actionList.push_back(protocol::network::packet::GameActionType::LEFT);
        _actionList.push_back(protocol::network::packet::GameActionType::LEFT);
        _actionList.push_back(protocol::network::packet::GameActionType::UP);
        _actionList.push_back(protocol::network::packet::GameActionType::UP);
        _actionList.push_back(protocol::network::packet::GameActionType::SHOOT);
        _actionList.push_back(protocol::network::packet::GameActionType::DOWN);
        _actionList.push_back(protocol::network::packet::GameActionType::DOWN);
        _actionList.push_back(protocol::network::packet::GameActionType::RIGHT);
        _actionList.push_back(protocol::network::packet::GameActionType::RIGHT);
    }
}

extern "C"
{
    std::shared_ptr<object::IObject>    init_libObj(std::string id)
    {
        return (std::make_shared<object::Monster>(id));
    }

    protocol::network::packet::GameObjectType getType()
    {
        return object::getObjType();
    }
}