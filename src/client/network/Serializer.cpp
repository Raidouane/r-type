//
// Created by raidouane on 16/01/18.
//

#include <iostream>
#include <string.h>
#include "client/network/Serializer.hpp"

namespace client::network {
    Serializer::Serializer() {}

    Serializer::~Serializer() {}

    std::array<std::uint8_t, 20> Serializer::to_array(const std::string &str) {
        std::array<std::uint8_t, 20> array = {};
        std::copy(std::begin(str), std::end(str), std::begin(array));

        return array;
    };


    std::shared_ptr<protocol::network::gameActionPacket> Serializer::serializeGameAction(
            protocol::network::packet::GameActionType type) {
        std::shared_ptr<protocol::network::gameActionPacket> packet;
        std::shared_ptr<protocol::network::packet::PacketHeader> header;
        std::shared_ptr<protocol::network::packet::gameAction> action;

        packet = std::make_shared<protocol::network::gameActionPacket>();
        header = std::make_shared<protocol::network::packet::PacketHeader>();
        action = std::make_shared<protocol::network::packet::gameAction>();

        memset(packet.get(), 0, sizeof(protocol::network::gameActionPacket));
        memset(header.get(), 0, sizeof(protocol::network::packet::PacketHeader));
        memset(action.get(), 0, sizeof(protocol::network::packet::gameAction));

        header->type = protocol::network::packet::PacketType::ACTION;
        action->type = type;
        packet->header = *header;
        packet->gameAction = *action;

        return packet;
    }

    std::shared_ptr<protocol::network::gameEventPacket> Serializer::serializeGameEvent(
            protocol::network::packet::GameEventType type) {
        std::shared_ptr<protocol::network::gameEventPacket> packet;
        std::shared_ptr<protocol::network::packet::PacketHeader> header;
        std::shared_ptr<protocol::network::packet::gameEvent> event;

        packet = std::make_shared<protocol::network::gameEventPacket>();
        header = std::make_shared<protocol::network::packet::PacketHeader>();
        event = std::make_shared<protocol::network::packet::gameEvent>();

        memset(packet.get(), 0, sizeof(protocol::network::gameEventPacket));
        memset(header.get(), 0, sizeof(protocol::network::packet::PacketHeader));
        memset(event.get(), 0, sizeof(protocol::network::packet::gameEvent));

        header->type = protocol::network::packet::PacketType::EVENT;
        event->type = type;
        packet->header = *header;
        packet->gameEvent = *event;

        return packet;
    }
}