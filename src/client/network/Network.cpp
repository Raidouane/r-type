//
// Created by raidouane on 21/01/18.
//

#include <boost/bind/bind.hpp>
#include <boost/asio/placeholders.hpp>
#include <iostream>
#include <thread>
#include "client/network/Network.hpp"

namespace client
{
    Network::Network(boost::asio::io_service &io_service,
                     const std::string &ip, const std::string &port,
                     std::shared_ptr<client::Game> game)
            : _socket(io_service)
    {
        _game = game;
        boost::asio::ip::udp::resolver resolver(io_service);
        boost::asio::ip::udp::resolver::query query(boost::asio::ip::udp::v4(), ip, port);

        _endpoint = *resolver.resolve(query);
        _socket.open(boost::asio::ip::udp::v4());
    }

    Network::~Network() {}

    void Network::send(const boost::asio::mutable_buffers_1 &data)
    {
        _socket.send_to(data, _endpoint);
    }

    void Network::start_receive()
    {
        memset(_recv_buffer, 0, MAX_LENGTH);
        boost::asio::ip::udp::endpoint sender_endpoint;
        _socket.receive_from(boost::asio::buffer(_recv_buffer, MAX_LENGTH), sender_endpoint);
        handle_receive();
        usleep(1000000);
    }

    void Network::handleGameObjects(const protocol::network::packet::object &data)
    {
        _game->addGameObject(data);
    }

    void Network::handle_receive()
    {
        protocol::network::packet::PacketHeader *dataHeader;

        dataHeader = reinterpret_cast<protocol::network::packet::PacketHeader *>(_recv_buffer);
        switch (dataHeader->type)
        {
            case protocol::network::packet::PacketType::IDENTITY:
                _id = std::string(reinterpret_cast<const char *>(dataHeader->idClient.data()));
                _game->setPlayerID(_id);
                break;
            case protocol::network::packet::PacketType::GameObject:
            {
                protocol::network::packet::object *object;
                object = reinterpret_cast<protocol::network::packet::object *>(_recv_buffer + sizeof(*dataHeader));

                handleGameObjects(*object);
                break;
            }
            default: {
                std::cout << "DEFAULT DATA\n";
            }
        }
        start_receive();
    }

    void Network::runReceiver()
    {
        _thread_receiver = std::thread(&Network::start_receive, this);
        _thread_receiver.detach();
    }
}