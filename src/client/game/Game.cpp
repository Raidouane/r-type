//
// Created by raidouane on 21/01/18.
//

#include "client/game/Game.hpp"

namespace client
{
    Game::Game(const std::string &name)
    {
        _name = name;
    }

    Game::~Game() {}

    void Game::init()
    {
        _gameObjects.clear();
    }

    std::map<std::string, protocol::network::packet::object> Game::getGameObjects() const
    {
        return _gameObjects;
    }

    void Game::removeGameObject(const protocol::network::packet::object &object)
    {
        const auto idArr = object.id;
        const auto idObject = std::string(reinterpret_cast<const char *>(idArr.data()));
        _gameObjects.erase(idObject);
    }

    void Game::addGameObject(const protocol::network::packet::object &object)
    {
        const auto idArr = object.id;
        const auto idObject = std::string(reinterpret_cast<const char *>(idArr.data()));
        _gameObjects[idObject] = object;
    }

    std::string Game::getName() const
    {
        return _name;
    }

    std::string Game::getPlayerID() const
    {
        return _playerID;
    }

    void Game::setPlayerID(const std::string &id)
    {
        _playerID = id;
    }
}