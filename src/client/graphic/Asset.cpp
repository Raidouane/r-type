//
// Created by raidouane on 21/01/18.
//

#include "client/graphic/Asset.hpp"

namespace client::graphic
{
    Asset::Asset() {}
    Asset::~Asset() {}

    void Asset::buildAssets()
    {
        _assets = {
                {protocol::network::packet::GameObjectType::BACKGROUND,
                        std::string("./src/client/graphic/assets/background/blue-space-field.jpg")},
                {protocol::network::packet::GameObjectType::PLAYER_1,
                        std::string("./src/client/graphic/assets/ships/player.png")},
                {protocol::network::packet::GameObjectType::PLAYER_2,
                        std::string("./src/client/graphic/assets/ships/blue-ship.png")},
                {protocol::network::packet::GameObjectType::PLAYER_3,
                        std::string("./src/client/graphic/assets/ships/millennium-falcon.png")},
                {protocol::network::packet::GameObjectType::PLAYER_4,
                        std::string("./src/client/graphic/assets/ships/phantom.png")},
                {protocol::network::packet::GameObjectType::MONSTER_LEVEL1,
                        std::string("./src/client/graphic/assets/aliens/grasp.png")},
                {protocol::network::packet::GameObjectType::ENEMY_MISSILE,
                        std::string("./src/client/graphic/assets/bullets/monster-bullet.png")},
                {protocol::network::packet::GameObjectType::PLAYER_MISSILE,
                        std::string("./src/client/graphic/assets/bullets/rasengan.png")},
                {protocol::network::packet::GameObjectType::STAGE_OBSTACLE,
                        std::string("./src/client/graphic/assets/obstacles/meteorit.png")},
        };
    }

    sf::Texture Asset::getTextureByGameObjectType(const protocol::network::packet::GameObjectType &type)
    {
        sf::Texture texture;
        auto &path = _assets[type];

        if (!texture.loadFromFile(path))
            throw Error("Error: Fail to load texture at path: " + path);

        return texture;
    }

    sf::Texture Asset::getProgressBar() const
    {
        sf::Texture texture;

        if (!texture.loadFromFile("./src/client/graphic/assets/progressBar/progress-bar.png"))
            throw Error("Error: Fail to load texture");

        return texture;
    }

    sf::Texture Asset::getTextureHeart() const
    {
        sf::Texture texture;

        if (!texture.loadFromFile("./src/client/graphic/assets/progressBar/heart.png"))
            throw Error("Error: Fail to load texture");

        return texture;
    }

    sf::Texture Asset::getStartGameButton() const
    {
        sf::Texture texture;

        if (!texture.loadFromFile("./src/client/graphic/assets/buttons/start-game-button.png"))
            throw Error("Error: Fail to load texture");

        return texture;
    }

    sf::Texture Asset::getExitButton() const
    {
        sf::Texture texture;

        if (!texture.loadFromFile("./src/client/graphic/assets/buttons/exit.png"))
            throw Error("Error: Fail to load texture");

        return texture;
    }

    sf::Texture Asset::getSettingsButton() const
    {
        sf::Texture texture;

        if (!texture.loadFromFile("./src/client/graphic/assets/buttons/settings-cog.png"))
            throw Error("Error: Fail to load texture");

        return texture;
    }

    sf::Texture Asset::getLogo() const
    {
        sf::Texture texture;

        if (!texture.loadFromFile("./src/client/graphic/assets/titles/logo.png"))
            throw Error("Error: Fail to load texture");

        return texture;
    }

    void Asset::setMusicType(Asset::SoundType type)
    {
        _soundType = type;
    };

    std::string Asset::getMenuMusicPath() const
    {
        std::string path;

        switch (_soundType)
        {
            case SoundType::CATS:
                path = "./src/client/graphic/assets/background/sounds/Space_Cats_Magic_Fly.ogg";
                break;
            case SoundType::VOCAL:
                path = "./src/client/graphic/assets/background/sounds/mortal-kombat.ogg";
                break;
            default:
                path = "./src/client/graphic/assets/background/sounds/sound-space-background.ogg";
                break;
        }
        return path;
    }

    std::string Asset::getGameMusicPath() const
    {
        std::string path;

        switch (_soundType)
        {
            case SoundType::CATS:
                path = "./src/client/graphic/assets/background/sounds/Space_Cats_Magic_Fly.ogg";
                break;
            case SoundType::VOCAL:
                path = "./src/client/graphic/assets/background/sounds/mortal-kombat.ogg";
                break;
            default:
                path = "./src/client/graphic/assets/background/sounds/ambiance.ogg";
                break;
        }
        return path;
    }

    std::string Asset::getClickSoundPath() const
    {
        std::string path;

        switch (_soundType)
        {
            /*case SoundType::PORN:
                path = "./src/client/graphic/assets/background/sounds/porn-sounds.ogg";
                break;
            case SoundType::VOCAL:
                path = "./src/client/graphic/assets/background/sounds/mortal-kombat.ogg";
                break;*/
            default:
                path = "./src/client/graphic/assets/clicks/space-click.wav";
                break;
        }
        return path;
    }

    sf::Font Asset::getFontSettings() const
    {
      sf::Font  font;

      if (!font.loadFromFile("./src/client/graphic/assets/fonts/Montserrat-Regular.ttf"))
          throw Error("Error: Fail to load font");

      return font;
    }
}
