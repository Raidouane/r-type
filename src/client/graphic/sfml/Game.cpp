//
// Created by raidouane on 20/01/18.
//

#include <string>
#include <sstream>
#include "client/graphic/sfml/Game.hpp"

namespace client::graphic::sfml
{
    Game::Game(client::graphic::sfml::App *app)
    {
        _app = app;
    }

    void Game::init()
    {
        _app->getAsset()->setMusicType(client::graphic::Asset::SoundType::NORMAL);
        _app->setMusicVolumeLevel(25);
        _musicBackground.openFromFile(_app->getAsset()->getGameMusicPath());
        _musicBackground.setVolume(_app->getMusicVolumeLevel());
        _musicBackground.play();
    }


    void Game::drawBackground()
    {
        auto texture = _app->getAsset()->getTextureByGameObjectType(protocol::network::packet::GameObjectType::BACKGROUND);
        sf::RectangleShape background(sf::Vector2f(WINDOW_WIDTH, WINDOW_HEIGHT));

        background.setTexture(&texture);
        _app->getWindow()->draw(background);
    }

    void Game::drawProgressBar(unsigned int lifePercentage)
    {
        unsigned int widthProgressBar = 300;
        unsigned int heightProgressBar = 60;
        unsigned int lifeWidth = (lifePercentage * (widthProgressBar - 82)) / 100;

        sf::RectangleShape progressBarContainer(sf::Vector2f(widthProgressBar, heightProgressBar));
        sf::RectangleShape heartShape(sf::Vector2f(45, 45));
        sf::RectangleShape progressBar(sf::Vector2f(lifeWidth, 10));

        sf::Vector2u sizeWindow = _app->getWindow()->getSize();
        sf::Vector2f positionProgressBarContainer((sizeWindow.x / 2) - (widthProgressBar / 2),
                                                  sizeWindow.y - heightProgressBar - 20);
        sf::Vector2f positionProgressBar(positionProgressBarContainer.x + 76, positionProgressBarContainer.y + 26);
        sf::Vector2f positionHeart(positionProgressBarContainer.x + 10, positionProgressBarContainer.y + 10);

        auto textureProgressBar = _app->getAsset()->getProgressBar();
        auto textureHeart = _app->getAsset()->getTextureHeart();

        progressBarContainer.setTexture(&textureProgressBar);
        progressBarContainer.setPosition(positionProgressBarContainer);
        heartShape.setTexture(&textureHeart);
        heartShape.setPosition(positionHeart);
        progressBar.setFillColor(sf::Color::Green);
        progressBar.setPosition(positionProgressBar);
        _app->getWindow()->draw(progressBarContainer);
        _app->getWindow()->draw(progressBar);
        _app->getWindow()->draw(heartShape);
    }

    void Game::drawScore(const std::string &score)
    {
        sf::Font font;
        font.loadFromFile("./src/client/graphic/assets/fonts/Montserrat-Regular.ttf");

        sf::Text text(score, font);
        text.setCharacterSize(30);
        text.setStyle(sf::Text::Bold);
        text.setFillColor(sf::Color::White);
        _app->getWindow()->draw(text);
    }

    void Game::drawPlayerItems(protocol::network::packet::object object)
    {
        std::stringstream ss;
        ss << object.score;
        drawScore(std::string("SCORE: ") + ss.str());
        drawProgressBar(object.lp);
    }

    void Game::drawGameObjects()
    {
        for(auto const &obj : _app->getGameData()->getGameObjects())
        {
            protocol::network::packet::object object = obj.second;

            switch (object.gameObjectState)
            {
                case protocol::network::packet::GameObjectState::DESTROYED:
                    _app->getGameData()->removeGameObject(object);
                    break;
                case protocol::network::packet::GameObjectState::EXPLOSED:
                    _app->getGameData()->removeGameObject(object);
                    break;
                default:
                {
                    auto texture = _app->getAsset()->getTextureByGameObjectType(object.gameObjectType);
                    sf::RectangleShape objectGameRect(sf::Vector2f(object.width, object.height));

                    objectGameRect.setTexture(&texture);
                    objectGameRect.setPosition(object.pos.x, object.pos.y);
                    _app->getWindow()->draw(objectGameRect);
                }
            }
            if (obj.first == _app->getGameData()->getPlayerID())
            {
                drawPlayerItems(object);
            }
        }
    }

    void Game::draw()
    {
        try
        {
            drawBackground();
            drawGameObjects();
        }
        catch (const Error &e) {
            std::cerr << e.what() << std::endl;
        }
    }

    void Game::display()
    {
        manageEvents();
        _app->getWindow()->clear();
        draw();
        _app->getWindow()->display();
    }

    void Game::endGame()
    {
    }

    void Game::manageEvents()
    {
        sf::Event event;

        _app->setEventAction(protocol::network::packet::GameActionType::UNDEFINED);
        while (_app->getWindow()->pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                _app->getWindow()->close();
            else if (event.type == sf::Event::KeyReleased)
            {
                switch (event.key.code)
                {
                    case sf::Keyboard::Escape:
                    {
                        _app->setRenderType(client::graphic::ILib::RenderType::MENU);
                        _musicBackground.stop();
                        break;
                    }
                    case sf::Keyboard::Right:
                        _app->setEventAction(protocol::network::packet::GameActionType::RIGHT);
                        break;
                    case sf::Keyboard::Left:
                        _app->setEventAction(protocol::network::packet::GameActionType::LEFT);
                        break;
                    case sf::Keyboard::Up:
                        _app->setEventAction(protocol::network::packet::GameActionType::UP);
                        break;
                    case sf::Keyboard::Down:
                        _app->setEventAction(protocol::network::packet::GameActionType::DOWN);
                        break;
                    case sf::Keyboard::Space:
                        _app->setEventAction(protocol::network::packet::GameActionType::SHOOT);
                        break;
                    default:
                        _app->setEventAction(protocol::network::packet::GameActionType::UNDEFINED);
                        break;
                }
            }
        }
    }
}