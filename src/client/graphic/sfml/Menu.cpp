//
// Created by raidouane on 21/01/18.
//

#include <unistd.h>
#include "client/graphic/sfml/Menu.hpp"
#include "client/graphic/sfml/App.hpp"

namespace client::graphic::sfml
{
    Menu::Menu(client::graphic::sfml::App *app)
    {
        _app = app;
        _clickSoundBuff.loadFromFile(_app->getAsset()->getClickSoundPath());
        _clickSound.setBuffer(_clickSoundBuff);
        initRender();
    }

    void Menu::initRender()
    {
        const auto sizeWindow = _app->getWindow()->getSize();

        _textureLogo = std::make_unique<sf::Texture>(_app->getAsset()->getLogo());
        const auto sizeLogo = sf::Vector2f(700, 300);
        _logo = sf::RectangleShape(sizeLogo);
        _logo.setTexture(&(*_textureLogo.get()));
        _logo.setPosition((sizeWindow.x / 2) - (sizeLogo.x / 2), 20);

        _textureStartGameButton = std::make_unique<sf::Texture>(_app->getAsset()->getStartGameButton());
        const auto sizeStartGameButton = sf::Vector2f(500, 200);
        _startGameButton = sf::RectangleShape(sizeStartGameButton);
        _startGameButton.setTexture(&(*_textureStartGameButton.get()));
        _startGameButton.setPosition((sizeWindow.x / 2) - (sizeStartGameButton.x / 2),
                                     (sizeWindow.y / 2) - (sizeStartGameButton.y / 2) - 20);

        _textureExitButton = std::make_unique<sf::Texture>(_app->getAsset()->getExitButton());
        const auto sizeExitButton = sf::Vector2f(150, 150);
        _exitButton = sf::RectangleShape(sizeExitButton);
        _exitButton.setTexture(&(*_textureExitButton.get()));
        _exitButton.setPosition((sizeWindow.x / 2) + sizeExitButton.x,
                                (sizeWindow.y - (sizeExitButton.y) * 2));

        _textureSettingsButton = std::make_unique<sf::Texture>(_app->getAsset()->getSettingsButton());
        const auto sizeSettingsButton = sf::Vector2f(150, 150);
        _settingsButton = sf::RectangleShape(sizeSettingsButton);
        _settingsButton.setTexture(&(*_textureSettingsButton.get()));
        _settingsButton.setPosition((sizeWindow.x / 2) - (sizeSettingsButton.x * 2),
                                    (sizeWindow.y - (sizeSettingsButton.y) * 2));

        _rectStartButton = sf::FloatRect(_startGameButton.getPosition().x,
                                         _startGameButton.getPosition().y,
                                         _startGameButton.getSize().x,
                                         _startGameButton.getSize().y);
        _rectSettingButton = sf::FloatRect(_settingsButton.getPosition().x,
                                           _settingsButton.getPosition().y,
                                           _settingsButton.getSize().x,
                                           _settingsButton.getSize().y);
        _rectExitButton = sf::FloatRect(_exitButton.getPosition().x,
                                        _exitButton.getPosition().y,
                                        _exitButton.getSize().x,
                                        _exitButton.getSize().y);
    }

    void Menu::drawBackground()
    {
        auto texture = _app->getAsset()->getTextureByGameObjectType(protocol::network::packet::GameObjectType::BACKGROUND);
        sf::RectangleShape background(sf::Vector2f(WINDOW_WIDTH, WINDOW_HEIGHT));

        background.setTexture(&texture);
        _app->getWindow()->draw(background);
    }

    void Menu::runSounds()
    {
        _musicBackground.openFromFile(_app->getAsset()->getMenuMusicPath());
        _musicBackground.play();
    }

    void Menu::init()
    {
        _app->setRenderType(client::graphic::ILib::RenderType::MENU);
        runSounds();
    }

    void Menu::drawButtons()
    {
        _app->getWindow()->draw(_logo);
        _app->getWindow()->draw(_startGameButton);
        _app->getWindow()->draw(_settingsButton);
        _app->getWindow()->draw(_exitButton);
    }

    void Menu::draw()
    {
        drawBackground();
        drawButtons();
    }

    void Menu::display()
    {
        manageEvents();
        _app->getWindow()->clear();
        draw();
        _app->getWindow()->display();
    }

    void Menu::manageEvents()
    {
        sf::Event event;

        while (_app->getWindow()->pollEvent(event))
        {
            switch (event.type)
            {
                case (sf::Event::Closed):
                {
                    _app->getWindow()->close();
                }
                case (sf::Event::KeyPressed):
                {
                    switch (event.key.code)
                    {
                        case sf::Keyboard::Escape:
                            _app->getWindow()->close();
                            break;
                        default:
                            break;
                    }
                }
                case (sf::Event::MouseButtonPressed):
                {
                    if (event.mouseButton.button == sf::Mouse::Left)
                    {

                        if (_rectStartButton.contains(event.mouseButton.x, event.mouseButton.y))
                        {
                            _clickSound.play();
                            _musicBackground.stop();
                            _app->setRenderType(client::graphic::ILib::RenderType::GAME);
                        }
                        else if (_rectExitButton.contains(event.mouseButton.x, event.mouseButton.y))
                        {
                            _clickSound.play();
                            usleep(1000000);
                            _app->getWindow()->close();
                        }
                        else if (_rectSettingButton.contains(event.mouseButton.x, event.mouseButton.y))
                        {
                            _app->setRenderType(client::graphic::ILib::RenderType::SETTINGS);
                            _clickSound.play();
                            _musicBackground.stop();
                        }
                    }
                }
                default:
                    break;
            }
        }
    }
}