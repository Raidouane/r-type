#include "client/graphic/sfml/Settings.hpp"
#include "client/graphic/sfml/App.hpp"
#include <unistd.h>

namespace client::graphic::sfml
{
    Settings::Settings(client::graphic::sfml::App *app)
    {
        _app = app;
        _musicBackground.openFromFile(_app->getAsset()->getMenuMusicPath());
    }

    void Settings::drawBackground()
    {
        auto texture = _app->getAsset()->getTextureByGameObjectType(protocol::network::packet::GameObjectType::BACKGROUND);
        sf::RectangleShape background(sf::Vector2f(WINDOW_WIDTH, WINDOW_HEIGHT));

        background.setTexture(&texture);
        _app->getWindow()->draw(background);
    }

    void Settings::initSoundItems()
    {
        _textFont = _app->getAsset()->getFontSettings();
        const auto sizeWindow = _app->getWindow()->getSize();

        sf::Text title = sf::Text("Sound", _textFont, 64);
        title.setFillColor(sf::Color::White);
        title.setStyle(sf::Text::Bold);
        title.setOrigin(title.getLocalBounds().left + title.getLocalBounds().width/2.0f,
                       title.getLocalBounds().top  + title.getLocalBounds().height/2.0f);
        title.setPosition(sf::Vector2f(sizeWindow.x/2.0f, 50));
        _soundsItems.push_back(title);

        int i = 1;
        int h = 150;
        for (const auto& [k, v] : _soundTypeName)
        {
            sf::Text tmpItem = sf::Text(v, _textFont);
            tmpItem.setFillColor(sf::Color::White);
            tmpItem.setOrigin(tmpItem.getLocalBounds().left + tmpItem.getLocalBounds().width/2.0f,
                           tmpItem.getLocalBounds().top  + tmpItem.getLocalBounds().height/2.0f);
            tmpItem.setPosition(sf::Vector2f(sizeWindow.x/4.0f * i, h));
            _soundsItems.push_back(tmpItem);
            h = (i < 5) ? h : (h + 50);
            i = (i == 4) ? 1 : (i + 1);
        }
    }

    void Settings::init()
    {
      const auto sizeWindow = _app->getWindow()->getSize();

      _soundTypeName[client::graphic::Asset::SoundType::NORMAL] = "Classic";
      _soundTypeName[client::graphic::Asset::SoundType::VOCAL] = "Vocal";
      _soundTypeName[client::graphic::Asset::SoundType::CATS] = "Space Cats";

      _musicBackground.play();
      _textureExitButton = std::make_unique<sf::Texture>(_app->getAsset()->getExitButton());
      const auto sizeExitButton = sf::Vector2f(150, 150);
      _exitButton = sf::RectangleShape(sizeExitButton);
      _exitButton.setTexture(&(*_textureExitButton.get()));
      _exitButton.setPosition((sizeWindow.x / 2) + sizeExitButton.x,
                              (sizeWindow.y - (sizeExitButton.y) * 2));

      _rectBackButton = sf::FloatRect(_exitButton.getPosition().x,
                                       _exitButton.getPosition().y,
                                       _exitButton.getSize().x,
                                      _exitButton.getSize().y);
      initSoundItems();
    }

    void Settings::drawButtons()
    {
        _app->getWindow()->draw(_exitButton);
    }

    void Settings::display()
    {
        manageEvents();
        _app->getWindow()->clear();
        draw();
        _app->getWindow()->display();
    }

    void Settings::drawSoundItems()
    {
      for (auto item : _soundsItems)
      {
          _app->getWindow()->draw(item);
      }
    }

    void Settings::draw()
    {
        drawBackground();
        drawButtons();
        drawSoundItems();
    }

    void Settings::setWhiteTitle()
    {
        std::vector<sf::Text>::iterator it;
        for (it = _soundsItems.begin(); it != _soundsItems.end(); ++it)
        {
          (*it).setFillColor(sf::Color::White);
        }
    }

    void Settings::manageEvents()
    {
        sf::Event event;

        while (_app->getWindow()->pollEvent(event))
        {
            switch (event.type)
            {
                case (sf::Event::Closed):
                {
                    _app->getWindow()->close();
                }
                case (sf::Event::KeyPressed):
                {
                    switch (event.key.code)
                    {
                        case sf::Keyboard::Escape:
                            _app->setRenderType(client::graphic::ILib::RenderType::MENU);
                            _musicBackground.stop();
                            break;
                        default:
                            break;
                    }
                }
                case (sf::Event::MouseButtonPressed):
                {
                    if (event.mouseButton.button == sf::Mouse::Left)
                    {
                        std::vector<sf::Text>::iterator it;
                        int i = 0;
                        for (it = _soundsItems.begin() + 1; it != _soundsItems.end(); ++it)
                        {
                          if ((*it).getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
                          {
                              setWhiteTitle();
                              (*it).setFillColor(sf::Color::Red);
                              _musicBackground.stop();
                              _app->getAsset()->setMusicType(static_cast<client::graphic::Asset::SoundType>(i));
                              _musicBackground.openFromFile(_app->getAsset()->getMenuMusicPath());
                              _musicBackground.play();
                          }
                          i++;
                        }
                        if (_rectBackButton.contains(event.mouseButton.x, event.mouseButton.y))
                        {
                            _app->setRenderType(client::graphic::ILib::RenderType::MENU);
                            _musicBackground.stop();
                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }
}
