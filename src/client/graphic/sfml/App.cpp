//
// Created by raidouane on 20/01/18.
//

#include <string>
#include <sstream>
#include <client/graphic/sfml/App.hpp>
#include "client/graphic/sfml/Game.hpp"
#include "client/graphic/sfml/Menu.hpp"
#include "client/graphic/sfml/Settings.hpp"

namespace client::graphic::sfml
{
    App::App(std::shared_ptr<client::Game> game)
    {
        _game = game;
        _asset = std::make_unique<client::graphic::Asset>();
        _asset->buildAssets();
        _asset->setMusicType(Asset::SoundType::NORMAL);
    }

    std::shared_ptr<client::graphic::Asset> App::getAsset() const { return _asset; }

    std::shared_ptr<sf::RenderWindow>       App::getWindow() const { return _window; }

    std::shared_ptr<client::Game>           App::getGameData() const { return _game; }

    client::graphic::ILib::RenderType App::getRenderType() const { return _renderType; }

    void App::setEventAction(protocol::network::packet::GameActionType actionType) { _eventAction = actionType; }

    void App::setRenderType(client::graphic::ILib::RenderType renderType) { _renderType = renderType; }

    void App::setMusicVolumeLevel(unsigned int level)
    {
        _musicVolumeLevel = level;
    }

    unsigned int App::getMusicVolumeLevel() const
    {
        return _musicVolumeLevel;
    }

    void App::init()
    {
        _window = std::make_shared<sf::RenderWindow>(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), _game->getName());
        _menuRender = std::make_unique<client::graphic::sfml::Menu>(this);
        _gameRender = std::make_unique<client::graphic::sfml::Game>(this);
        _settingsRender = std::make_unique<client::graphic::sfml::Settings>(this);
        _menuRender->init();
    }

    void App::initGame()
    {
        _gameRender->init();
    }

    void App::initSettings()
    {
        _settingsRender->init();
    }

    void App::initMenu()
    {
        _menuRender->init();
    }

    void App::displayGame()
    {
        _gameRender->display();
    }

    void App::displayMenu()
    {
        _menuRender->display();
    }

    void App::displaySettings()
    {
        _settingsRender->display();
    }

    bool App::isOpenWindow()
    {
        return _window->isOpen();
    }

    protocol::network::packet::GameActionType App::getAction() const { return _eventAction; }

}