#include <csignal>
#include <iostream>
#include <client/error/Error.hpp>
#include "client/core/Core.hpp"

void signal_handler(int sig) {}

int main(int ac, char **av, char **env)
{
    if (!env || !env[0] || !getenv("DISPLAY"))
    {
        std::cerr << "Env content missing." << std::endl;
        return (1);
    }
    signal(SIGINT, signal_handler);

    try {
        auto ip = (ac > 2) ? std::string(av[1]) : std::string("");
        auto port = (ac > 3) ? std::string(av[2]) : std::string("");

        client::Core *core = new client::Core(ip, port);
        core->run();
    }
    catch (Error &e)
    {
        std::cerr << e.what() << std::endl;
        return (1);
    }
    return (0);
}