//
// Created by raidouane on 20/01/18.
//

#include <client/graphic/sfml/App.hpp>
#include <client/error/Error.hpp>
#include <boost/array.hpp>
#include "client/core/Core.hpp"
#include "client/network/Serializer.hpp"
#include "protocol/Protocol.hpp"

namespace client
{
    Core::Core(const std::string &ipDestinator, const std::string &portDestinator)
    {
        boost::asio::io_service io_service;
        _serverIP = !ipDestinator.empty() ? ipDestinator : std::string("0.0.0.0");
        _serverPort = !portDestinator.empty() ? portDestinator :  std::string("4400");

        _game = std::make_shared<client::Game>(std::string("r-type"));
        _graphicLib = new client::graphic::sfml::App(_game);
        _network = std::make_unique<client::Network>(io_service, _serverIP, _serverPort, _game);
    }

    Core::~Core() {}

    void Core::runGame()
    {
        try {
            client::network::Serializer serializer;
            std::shared_ptr<protocol::network::gameEventPacket> packet;
            std::shared_ptr<protocol::network::gameActionPacket> gameActionPacket;
            packet = serializer.serializeGameEvent(protocol::network::packet::GameEventType::START_GAME);
            _network->send(boost::asio::buffer(packet.get(), sizeof(*packet)));

            _game->init();
            _graphicLib->initGame();
            while (_graphicLib->isOpenWindow() &&
                   _graphicLib->getRenderType() == client::graphic::ILib::RenderType::GAME)
            {
                _graphicLib->displayGame();
                auto act = _graphicLib->getAction();
                if (act != protocol::network::packet::GameActionType::UNDEFINED)
                {
                    gameActionPacket = serializer.serializeGameAction(_graphicLib->getAction());
                    _network->send(boost::asio::buffer(gameActionPacket.get(), sizeof(*gameActionPacket)));
                }
            }
            packet = serializer.serializeGameEvent(protocol::network::packet::GameEventType::END_GAME);
            _network->send(boost::asio::buffer(packet.get(), sizeof(*packet)));
        }
        catch (const Error &e)
        {
            throw e;
        }
    }

    void Core::runMenu()
    {
        _graphicLib->initMenu();
        while (_graphicLib->isOpenWindow() &&
               _graphicLib->getRenderType() == client::graphic::ILib::RenderType::MENU)
            _graphicLib->displayMenu();
    }

    void Core::runSettings()
    {
        _graphicLib->initSettings();
        while (_graphicLib->isOpenWindow() &&
               _graphicLib->getRenderType() == client::graphic::ILib::RenderType::SETTINGS)
            _graphicLib->displaySettings();
    }

    void Core::run()
    {
        try {
            _graphicLib->init();
            _network->runReceiver();
            while (_graphicLib->isOpenWindow())
            {
                switch (_graphicLib->getRenderType())
                {
                    case client::graphic::ILib::RenderType::GAME:
                        runGame();
                        break;
                    case client::graphic::ILib::RenderType::MENU:
                        runMenu();
                        break;
                    case client::graphic::ILib::RenderType::SETTINGS:
                        runSettings();
                        break;
                }
            }
        } catch (const Error &e){
            throw e;
        }
    }
}