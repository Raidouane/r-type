//
// Created by raidouane on 15/01/18.
//

#include <iostream>
#include <algorithm>
#include "server/core/RoomsManager.hpp"

namespace server::core
{
    RoomsManager::RoomsManager(std::shared_ptr<game::IGame> &game) : _game(game) {}

    RoomsManager::~RoomsManager() {}

    std::shared_ptr<server::network::Client> RoomsManager::getClient(const std::string &id) const
    {
        for (std::vector<std::shared_ptr<server::network::Client>>::const_iterator itClient = _clients.begin();
             itClient != _clients.end();
             itClient++)
        {
            if ((*itClient)->getId() == id)
                return *itClient;
        }
        return (nullptr);
    }

    std::shared_ptr<server::network::Room> RoomsManager::getIncompleteRoom()
    {
        for (auto room : _rooms) {
            if (room->getClients().size() < server::network::Room::MAX_CLIENTS) {
                return room;
            }
        }
        return (nullptr);
    }

    void RoomsManager::findRoom(std::shared_ptr<server::network::Client> client)
    {
        std::shared_ptr<server::network::Room> room;
        static int val = 0;

        if ((room = getIncompleteRoom())) {
            room->addClient(client);
        } else {
            std::shared_ptr<server::network::Room> newRoom = std::make_shared<server::network::Room>(val, _game);
            val++;

            newRoom->addClient(client);
            _rooms.push_back(newRoom);
            newRoom->run();
        }
    }

    void RoomsManager::newClientHandle(boost::asio::ip::udp::socket *socket,
                                       boost::asio::ip::udp::endpoint *remote_endpoint)
    {
        std::string ipAddress = remote_endpoint->address().to_string();
        unsigned short uiClientPort = remote_endpoint->port();
        std::string idClient = ipAddress + ':' + std::to_string(uiClientPort);

        if (!getClient(idClient))
        {
            std::shared_ptr<server::network::Client> newClient =
                    std::make_shared<server::network::Client>(socket, remote_endpoint, ipAddress,
                                                              std::to_string(uiClientPort));
            _clients.push_back(newClient);
            findRoom(newClient);
        }
    }

    void RoomsManager::removeRoom(server::network::Room *room)
    {
        std::vector<std::shared_ptr<server::network::Room>> newRooms;

        room->stop();
        for (auto itRoom : _rooms)
           if (itRoom.get() != room)
               newRooms.push_back(itRoom);
        _rooms = newRooms;
    }

    void RoomsManager::removeClient(std::shared_ptr<server::network::Client> client)
    {
        std::vector<std::shared_ptr<server::network::Client>> newClients;

        for (auto itClient : _clients)
            if (itClient != client)
                newClients.push_back(itClient);
        _clients = newClients;
    }

    void RoomsManager::removeClientHandle(boost::asio::ip::udp::endpoint *remote_endpoint)
    {
        std::shared_ptr<server::network::Client> client;
        std::string ipAddress = remote_endpoint->address().to_string();
        unsigned short uiClientPort = remote_endpoint->port();
        std::string idClient = ipAddress + ':' + std::to_string(uiClientPort);

        if ((client = getClient(idClient)))
        {
            server::network::Room *room = client->getRoom();

            room->getEngine()->removeObj(idClient);
            room->removeClient(client);
            if (room->getClients().empty())
                removeRoom(room);
            removeClient(client);
        }
    }

    void RoomsManager::eventsHandle(boost::asio::ip::udp::socket *socket,
                                   boost::asio::ip::udp::endpoint *remote_endpoint,
                                   protocol::network::packet::gameEvent *event)
    {
        switch (event->type)
        {
            case protocol::network::packet::GameEventType::START_GAME:
            {
                newClientHandle(socket, remote_endpoint);
                break;
            }
            case protocol::network::packet::GameEventType::END_GAME:
            {
                removeClientHandle(remote_endpoint);
                break;
            }
        }
    }

    void RoomsManager::actionsHandle(boost::asio::ip::udp::socket *socket,
                                    boost::asio::ip::udp::endpoint *remote_endpoint,
                                    protocol::network::packet::gameAction *action)
    {
//        std::shared_ptr<server::network::Client> client;
        std::string ipAddress = remote_endpoint->address().to_string();
        unsigned short uiClientPort = remote_endpoint->port();
        std::string idClient = ipAddress + ':' + std::to_string(uiClientPort);
        auto client = getClient(idClient);
        if (client) {
            client->getRoom()->newClientEvent(idClient, action->type);
        }
    }

    void RoomsManager::manageRooms(boost::asio::ip::udp::socket *socket,
                                   boost::asio::ip::udp::endpoint *remote_endpoint,
                                   char *data)
    {
        std::shared_ptr<server::network::Client> client;
        std::string ipAddress = remote_endpoint->address().to_string();
        unsigned short uiClientPort = remote_endpoint->port();
        std::string idClient = ipAddress + ':' + std::to_string(uiClientPort);

        protocol::network::packet::PacketHeader *dataHeader;

        dataHeader = reinterpret_cast<protocol::network::packet::PacketHeader *>(data);
        switch (dataHeader->type)
        {
            case protocol::network::packet::PacketType::EVENT:
            {
                protocol::network::packet::gameEvent *dataBody;

                dataBody = reinterpret_cast<protocol::network::packet::gameEvent *>(data + sizeof(*dataHeader));
                eventsHandle(socket, remote_endpoint, dataBody);
                break;
            }
            case protocol::network::packet::PacketType::ACTION:
            {
                protocol::network::packet::gameAction *dataBody;

                dataBody = reinterpret_cast<protocol::network::packet::gameAction *>(data + sizeof(*dataHeader));
                actionsHandle(socket, remote_endpoint, dataBody);
                break;
            }
            default:
                break;
        }
    }
}