
#include <ctime>
#include <iostream>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <thread>
#include "server/core/Core.hpp"

namespace server
{
    Core::Core(boost::asio::io_service &io_service)
            : _socket(io_service, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), 4400))
    {
        std::cout << "Server listen on port:  4400" << std::endl;
        memset(_recv_buffer, 0, max_length);
        _libManager = std::make_unique<core::LibManager>();
        _dirManager = std::make_unique<core::DirManager>();
        this->loadLibraries();
        _roomsManager = std::make_unique<core::RoomsManager>(_game);
        start_receive();
    }

    Core::~Core() {}

    void Core::start_receive() {
        memset(_recv_buffer, 0, max_length);
        _socket.async_receive_from(boost::asio::buffer(_recv_buffer), _remote_endpoint,
                                   boost::bind(&Core::handle_receive, this,
                                               boost::asio::placeholders::error));
    }

    void Core::handle_receive(const boost::system::error_code &error) {

        if (!error || error == boost::asio::error::message_size) {
            _roomsManager->manageRooms(&_socket, &_remote_endpoint, _recv_buffer);
            start_receive();
        }
    }

    void Core::handle_send(boost::shared_ptr<std::string> message,
                                   const boost::system::error_code &error) {

    }

    void Core::loadLibraries()
    {
        _libManager->loadLib(server::core::GAME, "lib/libgame.so");
        _game = _libManager->getGameLib();
        this->loadConstructors();
    }

    void Core::loadConstructors()
    {
        std::vector<std::string> list;
        _dirManager->generateFileList("lib");
        list = _dirManager->getPathList();

        for (auto path : list)
        {
            if (path != "lib/libgame.so") {
                _libManager->loadLib(server::core::OBJECT, path);
                _game->loadObjFactory(_libManager->getObjType(), _libManager->getObjConstructor());
            }
        }
    }
}