#include "server/core/LibManager.hpp"

namespace server::core
{
    LibManager::LibManager() : _gameConstructor(nullptr), _objectConstructor(nullptr) {}

    LibManager::~LibManager() {}

    void LibManager::loadLib(libType type, std::string targetPath)
    {
        if (type == libType::GAME)
            this->loadGameLib(targetPath);
        if (type == libType::OBJECT)
            this->loadObjConstructor(targetPath);
    }

    void LibManager::loadGameLib(std::string targetPath)
    {
        void *gameLibHandler = this->openLib(libType::GAME, RTLD_LAZY, targetPath);
        _gameConstructor = reinterpret_cast<std::shared_ptr<game::IGame> (*)()>(dlsym(gameLibHandler, "init_libGame"));
        if (_gameConstructor != nullptr)
            _gameLib = _gameConstructor();
    }

    void *LibManager::openLib(libType type, int flg, std::string targetPath)
    {
        if (type == libType::GAME)
            return dlopen(targetPath.c_str(), flg);
        if (type == libType::OBJECT)
            return dlopen(targetPath.c_str(), flg);
        return (nullptr);
    }

    void LibManager::loadObjConstructor(std::string targetPath)
    {
        void *objLibHandler = this->openLib(libType::GAME, RTLD_LAZY, targetPath);
        _objTypeGetter = reinterpret_cast<protocol::network::packet::GameObjectType (*)()>(dlsym(objLibHandler, "getType"));
        _objType = _objTypeGetter();
        _objectConstructor = reinterpret_cast<std::shared_ptr<object::IObject> (*)(std::string)>(dlsym(objLibHandler, "init_libObj"));
    }

    std::shared_ptr<game::IGame> LibManager::getGameLib()
    {
        return (_gameLib);
    }

    std::function<std::shared_ptr<object::IObject> (std::string)> LibManager::getObjConstructor()
    {
        return (_objectConstructor);
    }

    protocol::network::packet::GameObjectType LibManager::getObjType()
    {
        return (_objType);
    }
}