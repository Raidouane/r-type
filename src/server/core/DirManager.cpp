#include <iostream>
#include "server/core/DirManager.hpp"

namespace server::core
{
    DirManager::DirManager() {}

    DirManager::~DirManager() {}

    void DirManager::generateFileList(std::string folderName)
    {
        struct dirent* tmp;

        _pathList.clear();
        DIR* buff = opendir(folderName.c_str());
        while ((tmp = readdir(buff))) {
            if ((int)(std::string(tmp->d_name).find("lib")) >= 0) {
                _pathList.push_back(folderName + "/" + tmp->d_name);
            }
        }
    }

    bool DirManager::watchFolder(std::string folderName) const
    {
        int i = 0;
        struct dirent* tmp;

        DIR* buff = opendir(folderName.c_str());
        while ((tmp = readdir(buff)))
            if (std::string(tmp->d_name).find("lib") >= 0)
                ++i;
        return (_pathList.size() > i);
    }

    unsigned long DirManager::getNbFiles()
    {
        return (_pathList.size());
    }

    std::vector<std::string> DirManager::getPathList() const
    {
        return (_pathList);
    }

}