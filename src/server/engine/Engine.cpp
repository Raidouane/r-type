//
// Created by raidouane on 16/01/18.
//

#include <iostream>
#include <thread>
#include <unistd.h>
#include "server/engine/Engine.hpp"
#include "game/IGame.hpp"

namespace server
{
    Engine::Engine(int id, std::shared_ptr<game::IGame> & game, std::pair<std::size_t, std::size_t> mapBounds,
                   std::queue<protocol::network::objectPacket> &responseQ)
            : _game(game), _id(id), _mapBounds(mapBounds), _responseQ(responseQ)
    {
    }

    void Engine::run()
    {
        this->objectGestion();
    }

    void Engine::objectGestion()
    {
        for (std::vector<std::shared_ptr<object::IObject>>::iterator obj = _objList.begin();
            obj < _objList.end();
            ++obj)
            if (*obj && !(*obj)->isAlive())
                _objList.erase(obj);
        this->processObjectsEvent();
        this->fillResponseQ();
    }

    int Engine::getId() const
    {
        return (_id);
    }

    void Engine::moveObject(std::shared_ptr<object::IObject> &obj)
    {
        object::objectParams params = obj->getParams();
        object::objectPos    pos = obj->getPos();

        switch (params.orientation)
        {
            case object::objectOrientation::NORTH :
                pos.y -= 1 * params.speed;
                break;
            case object::objectOrientation::EST :
                pos.x += 1 * params.speed;
                break;
            case object::objectOrientation::SOUTH :
                pos.y += 1 * params.speed;
                break;
            case object::objectOrientation::WEST :
                pos.x -= 1 * params.speed;
                break;
            default:
                break;
        }

        this->objSetPos(obj, pos);
        this->collisionCheck(obj);
    }

    void Engine::removeObj(const std::string &id)
    {
        std::vector<std::shared_ptr<object::IObject>> newObjs;

        for (auto itObj : _objList)
            if (itObj->getId() != id)
                newObjs.push_back(itObj);
        _objList = newObjs;
    }

    void Engine::collisionCheck(std::shared_ptr<object::IObject> &obj)
    {
        for (auto currentObj : _objList)
        {
            if (obj->getId() != currentObj->getId())
            {
                if (this->checkObjPos(obj, currentObj)) {
                    _game->notifyCollision(obj, currentObj);
                }
            }
        }
    }

    void Engine::objSetPos(std::shared_ptr<object::IObject> &obj, object::objectPos pos)
    {
        if (pos.y < _mapBounds.second && pos.x < _mapBounds.first
            && ((pos.x + obj->getSize().first) < _mapBounds.first)
            && ((pos.y + obj->getSize().second) < _mapBounds.second)) {
            obj->setPos(pos);
        } else if (obj->getType() != protocol::network::packet::GameObjectType::PLAYER) {
            obj->setState(protocol::network::packet::GameObjectState::DESTROYED);
        }
    }

    bool Engine::hasCollision(std::size_t pos, std::size_t pos2, std::size_t size)
    {
        return (pos >= pos2 && pos <= (pos2 + size));
    }

    bool Engine::checkObjPos(std::shared_ptr<object::IObject> &obj,
                             std::shared_ptr<object::IObject> &currentObj)
    {
        object::objectPos currentPos = currentObj->getPos();
        std::pair<std::size_t, std::size_t> currentSize = currentObj->getSize();

        return ((this->hasCollision(obj->getPos().x, currentPos.x, currentSize.first))
                && this->hasCollision(obj->getPos().y, currentPos.y, currentSize.first)
                || (this->hasCollision(obj->getPos().x + obj->getSize().first,
                                       currentPos.x, currentSize.first))
                   && this->hasCollision(obj->getPos().y, currentPos.y, currentSize.first)
                || (this->hasCollision(obj->getPos().x, currentPos.x, currentSize.first))
                   && this->hasCollision(obj->getPos().y + obj->getSize().second,
                                         currentPos.y, currentSize.first)
                || (this->hasCollision(obj->getPos().x + obj->getSize().first,
                                       currentPos.x, currentSize.first))
                   && this->hasCollision(obj->getPos().y + obj->getSize().second,
                                         currentPos.y, currentSize.first));
    }

    void Engine::addPlayer(std::string id)
    {
        _game->newPlayer(id, _objList);
    }

    void Engine::eventObject(std::string id, protocol::network::packet::GameActionType type)
    {
        for (auto obj : _objList)
            if (obj->getId() == id)
                obj->setNextAction(type);
    }

    void Engine::fillResponseQ()
    {
        server::network::Serializer serializer;
        for (auto obj : _objList)
        {
            std::shared_ptr<protocol::network::objectPacket> packet;
            packet = serializer.serializeObjectPacket(obj);
            _responseQ.push(*packet.get());
        }
    }

    void Engine::processObjectsEvent()
    {
        int i = 0;
        for (auto obj : _objList)
        {
            if (obj && obj->isAlive()
                && obj->getNextAction() != protocol::network::packet::GameActionType::UNDEFINED)
            {
                _game->newObjectEvent(i, obj, _objList);
                this->moveObject(obj);
                obj->nextAction();
            }
            ++i;
        }
    }
}