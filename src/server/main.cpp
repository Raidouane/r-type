
#include <bits/exception.h>
#include <iostream>
#include <boost/asio.hpp>
#include "server/core/Core.hpp"

int main() {
    try
    {
        boost::asio::io_service io_service;
        server::Core server(io_service);
        io_service.run();
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    return 0;
}