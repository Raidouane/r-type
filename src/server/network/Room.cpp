//
// Created by raidouane on 15/01/18.
//

#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include "server/network/Room.hpp"
#include "server/network/Serializer.hpp"

namespace server::network
{
    Room::Room(int id, std::shared_ptr<game::IGame> &game) : _game(game) {
        _engine = std::make_shared<Engine>(id, _game, _game->getMapBounds(), _responseQ);
    }

    Room::~Room() {}

    std::vector<std::shared_ptr<Client>> Room::getClients() const
    {
        return _clients;
    }

    bool Room::addClient(std::shared_ptr<Client> client)
    {
        if (_clients.size() < MAX_CLIENTS) {
            client->setRoom(this);
            _clients.push_back(client);
            _engine->addPlayer(client->getId());

            return true;
        }
        return false;
    }

    void Room::removeClient(std::shared_ptr<server::network::Client> client)
    {
        std::vector<std::shared_ptr<server::network::Client>> newClients;

        for (auto itClient : _clients)
        {
            if (itClient->getId() != client->getId())
                newClients.push_back(itClient);
        }
        _clients = newClients;
    }

    std::shared_ptr<server::Engine> Room::getEngine() const
    {
        return _engine;
    }

    void Room::handleRoom()
    {
        while (_isRunning)
        {
            _engine->run();
            runWriter();
            usleep(100000);
        }
    }

    void Room::stop()
    {
        _isRunning = false;
        _roomThread.join();
    }

    void Room::run()
    {
        _isRunning = true;
        _roomThread = std::thread(&Room::handleRoom, this);
    }

    void Room::runWriter()
    {
        Serializer serializer;
        protocol::network::packet::PacketHeader header;
        protocol::network::objectPacket packet;

        for (; !_responseQ.empty(); _responseQ.pop())
            for (auto itClient : _clients)
            {
                header = serializer.serializeClientIdentity(itClient->getId());
                packet = _responseQ.front();

                itClient->send(boost::asio::buffer(&header, sizeof(header)));
                itClient->send(boost::asio::buffer(&packet, sizeof(packet)));
            }
    }

    void Room::newClientEvent(std::string id, protocol::network::packet::GameActionType type)
    {
        _engine->eventObject(id, type);
    }
}