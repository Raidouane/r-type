//
// Created by raidouane on 16/01/18.
//

#include <iostream>
#include <string.h>
#include <object/IObject.hpp>
#include "server/network/Serializer.hpp"

namespace server::network
{
    Serializer::Serializer() {}
    Serializer::~Serializer() {}

    std::array<std::uint8_t, 20> Serializer::to_array(const std::string &str)
    {
        std::array<std::uint8_t, 20> array = {};
        std::copy(std::begin(str), std::end(str), std::begin(array));

        return array;
    };

    std::shared_ptr<protocol::network::objectPacket> Serializer::serializeObjectPacket(
            const std::shared_ptr<object::IObject> &gameObject)
    {
        std::shared_ptr<protocol::network::objectPacket> packet;
        std::shared_ptr<protocol::network::packet::PacketHeader> header;
        std::shared_ptr<protocol::network::packet::object> object;
        std::shared_ptr<protocol::network::packet::objectPos> position;

        packet = std::make_shared<protocol::network::objectPacket>();
        header = std::make_shared<protocol::network::packet::PacketHeader>();
        object = std::make_shared<protocol::network::packet::object>();
        position = std::make_shared<protocol::network::packet::objectPos>();

        memset(packet.get(), 0, sizeof(protocol::network::objectPacket));
        memset(header.get(), 0, sizeof(protocol::network::packet::PacketHeader));
        memset(object.get(), 0, sizeof(protocol::network::packet::object));
        memset(position.get(), 0, sizeof(protocol::network::packet::objectPos));

        header->type = protocol::network::packet::PacketType::GameObject;
        position->x = static_cast<std::uint32_t>(gameObject->getPos().x);
        position->y = static_cast<std::uint32_t>(gameObject->getPos().y);
        position->z = static_cast<std::uint32_t>(gameObject->getPos().z);
        object->id = to_array(gameObject->getId());
        object->lp = static_cast<std::uint32_t>(gameObject->getLp());
        object->score = static_cast<std::uint32_t>(gameObject->getScore());
        object->power = static_cast<std::uint32_t>(gameObject->getPower());
        object->speed = static_cast<std::uint32_t>(gameObject->getParams().speed);
        object->width = static_cast<std::uint32_t>(gameObject->getSize().first);
        object->height = static_cast<std::uint32_t>(gameObject->getSize().second);
        object->gameObjectType = gameObject->getSubType();
        object->gameObjectState = gameObject->getState();
        object->pos = *position;
        packet->header = *header;
        packet->object = *object;

        return packet;
    }

    protocol::network::packet::PacketHeader Serializer::serializeClientIdentity(const std::string &id)
    {
        protocol::network::packet::PacketHeader header = {};

        header.type = protocol::network::packet::PacketType::IDENTITY;
        header.idClient = to_array(id);

        return header;
    }
}