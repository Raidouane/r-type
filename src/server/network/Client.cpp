//
// Created by raidouane on 16/01/18.
//

#include <iostream>
#include "server/network/Client.hpp"
#include "server/network/Room.hpp"

namespace server::network {
    Client::Client(boost::asio::ip::udp::socket *socket, boost::asio::ip::udp::endpoint *endpoint,
                   const std::string &host, const std::string &port) {
        _socket = socket;
        _remote_endpoint = boost::asio::ip::udp::endpoint(*endpoint);
        _id = host + ":" + port;
        _ip = host;
        _port = port;
    }

    Client::~Client() {}

    std::string Client::getId() {
        return _id;
    }

    void server::network::Client::send(const boost::asio::mutable_buffers_1 &data) {
        _socket->send_to(data, _remote_endpoint);
    }

    void server::network::Client::setRoom(server::network::Room *newRoom) {
        _room = newRoom;
    }

    server::network::Room *server::network::Client::getRoom() {
        return (_room);
    }
}