#include <iostream>
#include <sstream>
#include "game/Rtype.hpp"

namespace game
{
    Rtype::Rtype(const std::pair<std::size_t, std::size_t> bounds) : _mapBounds(bounds)
    {
        _collFcts[std::make_pair(protocol::network::packet::GameObjectType::PLAYER, protocol::network::packet::GameObjectType::STAGE_OBSTACLE)] =
                [this](std::shared_ptr<object::IObject> &obj1,
                       std::shared_ptr<object::IObject> &obj2){this->collPlayerToObstacle(obj1, obj2);};

        _collFcts[std::make_pair(protocol::network::packet::GameObjectType::PLAYER, protocol::network::packet::GameObjectType::ENEMY_MISSILE)] =
                [this](std::shared_ptr<object::IObject> &obj1,
                       std::shared_ptr<object::IObject> &obj2){this->collPlayerToBullet(obj1, obj2);};

        _collFcts[std::make_pair(protocol::network::packet::GameObjectType::MONSTER, protocol::network::packet::GameObjectType::STAGE_OBSTACLE)] =
                [this](std::shared_ptr<object::IObject> &obj1,
                       std::shared_ptr<object::IObject> &obj2){this->collMonsterToObstacle(obj1, obj2);};

        _collFcts[std::make_pair(protocol::network::packet::GameObjectType::MONSTER, protocol::network::packet::GameObjectType::PLAYER_MISSILE)] =
                [this](std::shared_ptr<object::IObject> &obj1,
                       std::shared_ptr<object::IObject> &obj2){this->collMonsterToBullet(obj1, obj2);};

        _collFcts[std::make_pair(protocol::network::packet::GameObjectType::PLAYER, protocol::network::packet::GameObjectType::BONUS)] =
                [this](std::shared_ptr<object::IObject> &obj1,
                       std::shared_ptr<object::IObject> &obj2){this->collPlayerToBonus(obj1, obj2);};
    }

    Rtype::~Rtype() {}


    void Rtype::setInitialGame(std::vector<std::shared_ptr<object::IObject> > &objList)
    {
        if (_objFactory.find(protocol::network::packet::GameObjectType::MONSTER) != _objFactory.end()){
            std::shared_ptr<object::IObject> obj;
            for (int i = 0; i < 4; ++i) {
                obj = _objFactory[protocol::network::packet::GameObjectType::MONSTER](this->genObjId(i));
                object::objectPos pos = obj->getPos();
                pos.y +=  (i * 250);
                obj->setPos(pos);
                objList.push_back(obj);
            }
        }
    }

    void Rtype::notifyCollision(std::shared_ptr<object::IObject> &obj1,
                                std::shared_ptr<object::IObject> &obj2)
    {
        if (_collFcts.find({obj1->getType(), obj2->getSubType()}) != _collFcts.end()) {
            _collFcts[{obj1->getType(), obj2->getSubType()}](obj1, obj2);
        }
    }

    std::pair<std::size_t, std::size_t> Rtype::getMapBounds() const
    {
        return (_mapBounds);
    }

    void Rtype::collPlayerToObstacle(std::shared_ptr<object::IObject> &player,
                                     std::shared_ptr<object::IObject> &obstacle)
    {
        player->setLp(0);
        player->setState(protocol::network::packet::GameObjectState::DESTROYED);
    }

    void Rtype::collPlayerToBullet(std::shared_ptr<object::IObject> &player,
                                   std::shared_ptr<object::IObject> &bullet)
    {
        if (bullet->getSubType() == protocol::network::packet::GameObjectType::ENEMY_MISSILE)
        {
            player->setLp(player->getLp() - 1);
            if (player->getLp() == 0)
                player->setState(protocol::network::packet::GameObjectState::DESTROYED);
            bullet->setState(protocol::network::packet::GameObjectState::DESTROYED);
        }
    }

    void Rtype::collMonsterToObstacle(std::shared_ptr<object::IObject> &monster,
                                      std::shared_ptr<object::IObject> &)
    {
        monster->setLp(0);
        monster->setState(protocol::network::packet::GameObjectState::DESTROYED);
    }

    void Rtype::collPlayerToBonus(std::shared_ptr<object::IObject> &,
                                  std::shared_ptr<object::IObject> &)
    {
        //TODO: to be continued
    }

    void Rtype::collMonsterToBullet(std::shared_ptr<object::IObject> &monster,
                                    std::shared_ptr<object::IObject> &bullet)
    {
        if ((bullet->getParentObj() != nullptr)
            && (bullet->getSubType() == protocol::network::packet::GameObjectType::PLAYER_MISSILE))
        {
            monster->setLp(monster->getLp() - 1);
            bullet->getParentObj()->setScore(bullet->getParentObj()->getScore() + monster->getScore());
            if (monster->getLp() == 0)
                monster->setState(protocol::network::packet::GameObjectState::DESTROYED);
            bullet->setState(protocol::network::packet::GameObjectState::DESTROYED);
        }
    }

    void Rtype::loadObjFactory(protocol::network::packet::GameObjectType type, std::function<std::shared_ptr<object::IObject> (std::string)> c){
        _objFactory[type] = c;
    }

    void Rtype::newObjectEvent(int index, std::shared_ptr<object::IObject> &obj,
                               std::vector<std::shared_ptr<object::IObject> > &objList)
    {
        switch (obj->getNextAction()) {
            case protocol::network::packet::GameActionType::UP :
                obj->setOrientation(object::objectOrientation::NORTH);
                break;
            case protocol::network::packet::GameActionType::DOWN :
                obj->setOrientation(object::objectOrientation::SOUTH);
                break;
            case protocol::network::packet::GameActionType::LEFT :
                obj->setOrientation(object::objectOrientation::WEST);
                break;
            case protocol::network::packet::GameActionType::RIGHT :
                obj->setOrientation(object::objectOrientation::EST);
                break;
            case protocol::network::packet::GameActionType::SHOOT : {
                obj->setOrientation(object::objectOrientation::UNDEFINED);
                this->newBullet(this->genObjId(index), objList, obj);
                break;
            }
            default:
                break;
        }
    }

    void Rtype::newPlayer(std::string id, std::vector<std::shared_ptr<object::IObject> > &objList)
    {
        int i = 1;
        auto objP = _objFactory[protocol::network::packet::GameObjectType::PLAYER](id);

        for (auto &&obj : objList)
            if (obj->getType() == protocol::network::packet::GameObjectType::PLAYER)
                ++i;

        if (i == 1)
            this->setInitialGame(objList);

        objP->setSubType((protocol::network::packet::GameObjectType)(i));
        objList.push_back(objP);
    }

    void Rtype::newBullet(std::string id, std::vector<std::shared_ptr<object::IObject> > &objList,
                          std::shared_ptr<object::IObject> &parentObj)
    {
        auto obj = _objFactory[protocol::network::packet::GameObjectType::MISSILE](id);
        object::objectPos pos = parentObj->getPos();

        if (parentObj->getType() == protocol::network::packet::GameObjectType::PLAYER)
        {
            obj->setSubType(protocol::network::packet::GameObjectType::PLAYER_MISSILE);
            pos.x += 120;
        }

        if (parentObj->getType() == protocol::network::packet::GameObjectType::MONSTER)
        {
            obj->setSubType(protocol::network::packet::GameObjectType::ENEMY_MISSILE);
            pos.x -= obj->getSize().first;
        }

        obj->setParentObj(parentObj);
        obj->setPos(pos);
        std::vector<std::shared_ptr<object::IObject> > objNewList;
        objList.push_back(obj);
    }

    std::string Rtype::genObjId(int serie)
    {
        srand(time(NULL));
        std::stringstream ss;
        ss << rand() % 100000 + serie;
        return (ss.str());
    }
}

extern "C"
{
    std::shared_ptr<game::IGame>    init_libGame()
    {
        return (std::make_shared<game::Rtype>(std::make_pair(2100,1080)));
    }
}