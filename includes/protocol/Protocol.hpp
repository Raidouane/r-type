//
// Created by raidouane on 18/01/18.
//

#ifndef CPP_RTYPE_PROTOCOL_HPP
#define CPP_RTYPE_PROTOCOL_HPP

#include "Packet.hpp"

namespace protocol::network
{
    struct objectPacket
    {
        protocol::network::packet::PacketHeader header;
        protocol::network::packet::object object;
    };

    struct gameActionPacket
    {
        protocol::network::packet::PacketHeader header;
        protocol::network::packet::gameAction gameAction;
    };

    struct gameEventPacket
    {
        protocol::network::packet::PacketHeader header;
        protocol::network::packet::gameEvent gameEvent;
    };
}

#endif //CPP_RTYPE_PROTOCOL_HPP
