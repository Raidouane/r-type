//
// Created by raidouane on 18/01/18.
//

#ifndef CPP_RTYPE_PACKET_HPP
#define CPP_RTYPE_PACKET_HPP

#pragma once

#include <cstdint>
#include <array>
#include <stdint-gcc.h>

#if defined __GNUC__ || defined __clang__
#define PACK_STRUCT(declaration) declaration __attribute__((__packed__))
#elif defined _MSC_VER
#define PACK_STRUCT(declaration) __pragma(pack(push, 1)) declaration __pragma(pack(pop))
#else
#pragma "Compiler not supported."
#endif

namespace protocol::network::packet
{
    using ClientIdArray = std::array<std::uint8_t, 20>;

    enum class PacketType : std::uint8_t
    {
        GameObject = 0,
        CONFIGFILE,
        NEWPLAYER,
        ASSETS,
        ACTION,
        EVENT,
        IDENTITY,
    };

    enum class GameObjectType : std::uint8_t
    {
        BACKGROUND = 0,
        PLAYER_1,
        PLAYER_2,
        PLAYER_3,
        PLAYER_4,
        MONSTER_LEVEL1,
        ENEMY_MISSILE,
        PLAYER_MISSILE,
        STAGE_OBSTACLE,
        PLAYER,
        MONSTER,
        MISSILE,
        BONUS,
    };

    enum class GameObjectState : std::uint8_t
    {
        ALIVE,
        EXPLOSED,
        DESTROYED
    };

    enum class GameActionType : std::uint8_t
    {
        UNDEFINED = 0,
        UP,
        RIGHT,
        DOWN,
        LEFT,
        SHOOT,
    };

    enum class GameEventType : std::uint8_t
    {
        START_GAME = 0,
        END_GAME,
    };

    PACK_STRUCT(
            struct PacketHeader
            {
                PacketType        type;
                ClientIdArray     idClient;
            });
    static_assert(sizeof(PacketHeader) == sizeof(std::uint8_t) + sizeof(ClientIdArray),
                  "Invalid PacketHeader size");

    PACK_STRUCT(
            struct objectPos
            {
                std::uint32_t x;
                std::uint32_t y;
                std::uint32_t z;
            });
    static_assert(sizeof(objectPos) == sizeof(std::uint32_t) * 3,
                  "Invalid objectPos size");

    PACK_STRUCT(
            struct object
            {
                ClientIdArray     id;
                std::uint32_t     lp;
                std::uint32_t     score;
                std::uint32_t     power;
                std::uint32_t     speed;
                std::uint32_t     width;
                std::uint32_t     height;
                objectPos         pos;
                GameObjectType    gameObjectType;
                GameObjectState   gameObjectState;
//                server::object::objOrientation  orientation;
            });
    static_assert(sizeof(object) == sizeof(std::uint32_t) * 9 + sizeof(ClientIdArray) + sizeof(GameObjectType) + sizeof(GameObjectState),
                  "Invalid object size");

    PACK_STRUCT(
            struct gameAction
            {
              GameActionType type;
            });
    static_assert(sizeof(gameAction) == sizeof(std::uint8_t),
                  "Invalid gameAction size");

    PACK_STRUCT(
            struct gameEvent
            {
                GameEventType type;
            });
    static_assert(sizeof(gameEvent) == sizeof(std::uint8_t),
                  "Invalid gameEvent size");

}

#endif //CPP_RTYPE_PACKET_HPP
