#ifndef IGAME_HPP
#define IGAME_HPP

#include <vector>
#include <memory>
#include <functional>
#include "object/IObject.hpp"
#include "protocol/Protocol.hpp"

namespace game
{
    class IGame
    {

    public:
        IGame() {};
        virtual ~IGame() {};

        virtual void    newPlayer(std::string id, std::vector<std::shared_ptr<object::IObject> > &objList) = 0;
        virtual void    setInitialGame(std::vector<std::shared_ptr<object::IObject> > &objList) = 0;
        virtual void    notifyCollision(std::shared_ptr<object::IObject> &obj1, std::shared_ptr<object::IObject> &obj2) = 0;
        virtual void    loadObjFactory(protocol::network::packet::GameObjectType, std::function<std::shared_ptr<object::IObject> (std::string)>) = 0;
        virtual void    newBullet(std::string id, std::vector<std::shared_ptr<object::IObject> > &objList,
                                std::shared_ptr<object::IObject> &parentObj) = 0;
        virtual void    newObjectEvent(int i, std::shared_ptr<object::IObject> &obj,
                                   std::vector<std::shared_ptr<object::IObject> > &objList) = 0;


        virtual std::pair<std::size_t, std::size_t>     getMapBounds() const = 0;
    };
}

#endif