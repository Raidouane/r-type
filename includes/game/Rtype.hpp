#ifndef RTYPE_HPP
#define RTYPE_HPP

#include "IGame.hpp"
#include "object/IObject.hpp"
#include <map>
#include <functional>
#include "protocol/Packet.hpp"

using collisionPair = std::pair<protocol::network::packet::GameObjectType, protocol::network::packet::GameObjectType>;
using collisionPairFunc = void (std::shared_ptr<object::IObject> &obj1, std::shared_ptr<object::IObject> &obj2);
using factoryPrototype = std::shared_ptr<object::IObject> (std::string);

namespace game
{
    class Rtype : public IGame
    {
        std::map<collisionPair, std::function<collisionPairFunc>> _collFcts;
        std::pair<std::size_t, std::size_t>                       _mapBounds;
        std::map<protocol::network::packet::GameObjectType, std::function<factoryPrototype>> _objFactory;

    public:
        Rtype(const std::pair<std::size_t, std::size_t>);
        virtual ~Rtype();

        virtual void    newPlayer(std::string id, std::vector<std::shared_ptr<object::IObject> > &objList);
        virtual void    newBullet(std::string id, std::vector<std::shared_ptr<object::IObject> > &objList,
                                  std::shared_ptr<object::IObject>&);

        virtual void    setInitialGame(std::vector<std::shared_ptr<object::IObject> > &objList);
        virtual void    notifyCollision(std::shared_ptr<object::IObject> &obj1, std::shared_ptr<object::IObject> &obj2);
        virtual void    loadObjFactory(protocol::network::packet::GameObjectType, std::function<factoryPrototype>);
        virtual void             newObjectEvent(int i, std::shared_ptr<object::IObject> &obj,
                                   std::vector<std::shared_ptr<object::IObject> > &objList);

        virtual std::pair<std::size_t, std::size_t>     getMapBounds() const;
        virtual std::string                             genObjId(int serie = 0);



    private:
        void collPlayerToObstacle(std::shared_ptr<object::IObject> &, std::shared_ptr<object::IObject> &);
        void collPlayerToBullet(std::shared_ptr<object::IObject> &, std::shared_ptr<object::IObject> &);
        void collPlayerToBonus(std::shared_ptr<object::IObject> &, std::shared_ptr<object::IObject> &);
        void collMonsterToObstacle(std::shared_ptr<object::IObject> &, std::shared_ptr<object::IObject> &);
        void collMonsterToBullet(std::shared_ptr<object::IObject> &, std::shared_ptr<object::IObject> &);

    };
}

#endif