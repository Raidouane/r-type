//
// Created by raidouane on 21/01/18.
//

#ifndef CPP_RTYPE_GAME_HPP
#define CPP_RTYPE_GAME_HPP

# include <queue>
# include <map>
# include "protocol/Protocol.hpp"

namespace client
{
    class Game
    {
    private:
/*
        std::queue<protocol::network::packet::object> _gameObjectsQ;
*/
        std::map<std::string, protocol::network::packet::object> _gameObjects;
        std::string                                     _name;
        std::string                                     _playerID;

    public:
        Game(const std::string &);
        ~Game();

        void addGameObject(const protocol::network::packet::object &);
        void init();
        void removeGameObject(const protocol::network::packet::object &);
        std::map<std::string, protocol::network::packet::object> getGameObjects() const ;
        std::string getName() const;
        std::string getPlayerID() const;
        void setPlayerID(const std::string &);
    };
}

#endif //CPP_RTYPE_GAME_HPP
