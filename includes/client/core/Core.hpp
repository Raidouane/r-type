//
// Created by raidouane on 20/01/18.
//

#ifndef CPP_RTYPE_CORE_HPP
#define CPP_RTYPE_CORE_HPP

# include <bits/unique_ptr.h>
# include "client/graphic/ILib.hpp"
# include "client/game/Game.hpp"
# include "client/network/Network.hpp"

namespace client
{
    class Core
    {
        std::shared_ptr<client::Game>      _game;
        client::graphic::ILib              *_graphicLib;
        std::unique_ptr<client::Network>   _network;
        std::string                        _serverIP;
        std::string                        _serverPort;

    public:
        Core(const std::string &, const std::string &);
        ~Core();

        void run();
        void runGame();
        void runMenu();
        void runSettings();
    };
}

#endif //CPP_RTYPE_CORE_HPP
