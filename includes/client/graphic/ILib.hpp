#ifndef ILIB_HPP_
#define ILIB_HPP_
#define WINDOW_HEIGHT 1080
#define WINDOW_WIDTH 1920

#include <iostream>
#include <stdint.h>
#include <vector>
#include "protocol/Protocol.hpp"

namespace client::graphic
{
  class ILib
  {
  public:
      enum class RenderType
      {
          MENU = 0,
          GAME,
          SETTINGS,
      };

  public:
      virtual void init() = 0;
      virtual void initGame() = 0;
      virtual void initMenu() = 0;
      virtual void initSettings() = 0;
      virtual void displayGame() = 0;
      virtual void displayMenu() = 0;
      virtual client::graphic::ILib::RenderType getRenderType() const = 0;
      virtual void displaySettings() = 0;
      virtual bool isOpenWindow() = 0;
      virtual protocol::network::packet::GameActionType getAction() const = 0;
  };
}
#endif
