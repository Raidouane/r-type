//
// Created by raidouane on 20/01/18.
//

#ifndef CPP_RTYPE_SFML_GAME_HPP
#define CPP_RTYPE_SFML_GAME_HPP

# include <SFML/Graphics.hpp>
# include <SFML/Audio.hpp>
# include "client/game/Game.hpp"
# include "client/graphic/sfml/App.hpp"
# include "protocol/Protocol.hpp"

namespace client::graphic::sfml
{
    class Game
    {
    private:
        client::graphic::sfml::App                    *_app;
        sf::Music                                     _musicBackground;

    public:
        Game(client::graphic::sfml::App *);

        void init();
        void draw();
        void display();
        void endGame();
        void drawBackground();
        void drawGameObjects();
        void drawPlayerItems(protocol::network::packet::object);
        void drawProgressBar(unsigned int);
        void drawScore(const std::string &);
        void manageEvents();
    };
}

#endif //CPP_RTYPE_SFML_GAME_HPP
