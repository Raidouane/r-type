//
// Created by raidouane on 20/01/18.
//

#ifndef CPP_RTYPE_MENU_HPP
#define CPP_RTYPE_MENU_HPP

# include <SFML/Graphics.hpp>
# include <memory>
#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Music.hpp>
# include "client/graphic/sfml/App.hpp"

namespace client::graphic::sfml
{
    class Menu
    {
    private:
        client::graphic::sfml::App                    *_app;
        sf::SoundBuffer                               _clickSoundBuff;
        sf::Sound                                     _clickSound;
        sf::Music                                     _musicBackground;
        std::unique_ptr<sf::Texture>                  _textureLogo;
        std::unique_ptr<sf::Texture>                  _textureStartGameButton;
        std::unique_ptr<sf::Texture>                  _textureExitButton;
        std::unique_ptr<sf::Texture>                  _textureSettingsButton;
        sf::RectangleShape                            _logo;
        sf::RectangleShape                            _startGameButton;
        sf::RectangleShape                            _exitButton;
        sf::RectangleShape                            _settingsButton;
        sf::FloatRect                                 _rectStartButton;
        sf::FloatRect                                 _rectExitButton;
        sf::FloatRect                                 _rectSettingButton;

    public:
        Menu(client::graphic::sfml::App *);

        void draw();
        void drawBackground();
        void drawButtons();
        void manageEvents();
        void display();
        void init();
        void runSounds();
        void initRender();
    };
}

#endif //CPP_RTYPE_MENU_HPP
