//
// Created by raidouane on 20/01/18.
//

#ifndef CPP_RTYPE_SFML_HPP
#define CPP_RTYPE_SFML_HPP

# include <SFML/Graphics.hpp>
# include <SFML/Audio.hpp>
#include <queue>
# include "client/graphic/ILib.hpp"
# include "client/graphic/Asset.hpp"
# include "client/game/Game.hpp"
# include "protocol/Protocol.hpp"

namespace client::graphic::sfml
{
    class Game;
    class Menu;
    class Settings;

    class App : public ILib
    {
    private:
        std::shared_ptr<client::graphic::Asset>           _asset;
        protocol::network::packet::GameActionType         _eventAction;
        unsigned int                                      _musicVolumeLevel;
        std::shared_ptr<client::Game>                     _game;
        client::graphic::ILib::RenderType                 _renderType;
        std::unique_ptr<client::graphic::sfml::Menu>      _menuRender;
        std::unique_ptr<client::graphic::sfml::Game>      _gameRender;
        std::unique_ptr<client::graphic::sfml::Settings>  _settingsRender;
        std::shared_ptr<sf::RenderWindow>                 _window;

    public:
        App(std::shared_ptr<client::Game>);

        virtual void init();
        virtual void initGame();
        virtual void initSettings();
        virtual void initMenu();
        virtual void displayGame();
        virtual bool isOpenWindow();
        virtual client::graphic::ILib::RenderType getRenderType() const ;
        virtual protocol::network::packet::GameActionType getAction() const;
        virtual void displayMenu();
        virtual void displaySettings();

        std::shared_ptr<client::graphic::Asset> getAsset() const;
        std::shared_ptr<sf::RenderWindow>       getWindow() const;
        std::shared_ptr<client::Game>           getGameData() const;
        void                                    setEventAction(protocol::network::packet::GameActionType);
        void                                    setRenderType(client::graphic::ILib::RenderType);
        unsigned int                            getMusicVolumeLevel() const;
        void                                    setMusicVolumeLevel(unsigned int);
    };
}

#endif //CPP_RTYPE_SFML_HPP
