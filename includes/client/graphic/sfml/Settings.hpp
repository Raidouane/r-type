#ifndef CPP_RTYPE_SETTINGS_HPP
#define CPP_RTYPE_SETTINGS_HPP

# include <SFML/Graphics.hpp>
# include <memory>
# include "client/graphic/Asset.hpp"
#include "App.hpp"

#define MAX_NUMBER_OF_ITEMS 3

namespace client::graphic::sfml
{
    class Settings
    {
    private:
        client::graphic::sfml::App                          *_app;
        std::unique_ptr<sf::Texture>                        _textureExitButton;
        sf::RectangleShape                                  _exitButton;
        sf::Music                                           _musicBackground;
        sf::FloatRect                                       _rectBackButton;
        // sf::FloatRect                                       _rectMusique1Button;
        // sf::FloatRect                                       _rectMusique2Button;
        std::vector<sf::Text>                               _soundsItems;
        sf::Font                                            _textFont;
        std::map<client::graphic::Asset::SoundType, std::string> _soundTypeName;



    public:
        Settings(client::graphic::sfml::App *);

        void draw();
        void drawBackground();
        void drawButtons();
        void drawSoundItems();
        void manageEvents();
        void init();
        void initSoundItems();
        void display();
        void setWhiteTitle();
    };
}

#endif //CPP_RTYPE_SETTINGS_HPP
