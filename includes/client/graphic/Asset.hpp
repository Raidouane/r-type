//
// Created by raidouane on 21/01/18.
//

#ifndef CPP_RTYPE_ASSET_HPP
#define CPP_RTYPE_ASSET_HPP

# include <memory>
# include <map>
# include <SFML/Graphics.hpp>
# include "client/error/Error.hpp"
# include "protocol/Packet.hpp"

namespace client::graphic
{
    class Asset
    {
    public:
        enum class SoundType
        {
            NORMAL = 0,
            CATS,
            VOCAL,
        };
    private:
        std::map<protocol::network::packet::GameObjectType , std::string> _assets;
        SoundType                                                         _soundType;

    public:
        Asset();
        ~Asset();

        sf::Texture getTextureByGameObjectType(const protocol::network::packet::GameObjectType &);
        sf::Texture getTextureHeart() const;
        sf::Texture getProgressBar() const;
        sf::Texture getStartGameButton() const;
        sf::Texture getExitButton() const;
        sf::Texture getSettingsButton() const;
        sf::Texture getLogo() const;
        std::string getMenuMusicPath() const;
        std::string getGameMusicPath() const;
        std::string getClickSoundPath() const;
        sf::Font    getFontSettings() const;
        void buildAssets();
        void setMusicType(Asset::SoundType);
    };
}

#endif //CPP_RTYPE_ASSET_HPP
