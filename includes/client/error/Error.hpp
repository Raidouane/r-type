//
// Created by raidouane on 20/01/18.
//

#ifndef CPP_RTYPE_ERROR_HPP
#define CPP_RTYPE_ERROR_HPP

# include <string>
# include <exception>

class Error : public std::exception {
protected:
    std::string msg;
public:
    Error(std::string msg) { this->msg = msg; }

    virtual const char *what() const throw() {
        return this->msg.c_str();
    }
};

#endif //CPP_RTYPE_ERROR_HPP
