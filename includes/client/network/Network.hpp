//
// Created by raidouane on 21/01/18.
//

#ifndef CPP_RTYPE_NETWORK_HPP
#define CPP_RTYPE_NETWORK_HPP

class error;

# include <boost/asio/ip/udp.hpp>
#include <thread>
# include "client/game/Game.hpp"

namespace client
{
    class Network
    {
    private:
        enum {
            MAX_LENGTH = 2048
        };
    private:
        boost::asio::ip::udp::endpoint  _endpoint;
        std::shared_ptr<client::Game>   _game;
        char                            _recv_buffer[MAX_LENGTH];
        boost::asio::ip::udp::socket    _socket;
        std::thread                     _thread_receiver;
        std::string                     _id;

    public:
        Network(boost::asio::io_service &io_service, const std::string &ip, const std::string &port,
                std::shared_ptr<client::Game>);
        ~Network();

        void start_receive();
        void handle_receive();
        void handleGameObjects(const protocol::network::packet::object &);
        void send(const boost::asio::mutable_buffers_1 &data);
        void runReceiver();
    };
}

#endif //CPP_RTYPE_NETWORK_HPP
