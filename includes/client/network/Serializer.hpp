//
// Created by raidouane on 21/01/18.
//

#ifndef CPP_RTYPE_SERIALIZER_HPP
#define CPP_RTYPE_SERIALIZER_HPP

#include <string>
#include <memory>
#include "protocol/Protocol.hpp"

namespace client::network {

    class Serializer {
    public:
        Serializer();
        ~Serializer();

        std::array<std::uint8_t, 20> to_array(const std::string &);
        std::shared_ptr<protocol::network::gameActionPacket> serializeGameAction(protocol::network::packet::GameActionType);
        std::shared_ptr<protocol::network::gameEventPacket> serializeGameEvent(protocol::network::packet::GameEventType);
    };
}

#endif //CPP_RTYPE_SERIALIZER_HPP
