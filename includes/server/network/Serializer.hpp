//
// Created by raidouane on 16/01/18.
//

#ifndef CPP_RTYPE_SERIALIZER_HPP
#define CPP_RTYPE_SERIALIZER_HPP

#include <string>
#include <memory>
#include "object/IObject.hpp"
#include "protocol/Protocol.hpp"

namespace server::network {

    class Serializer {
    public:
        Serializer();
        ~Serializer();

        std::array<std::uint8_t, 20> to_array(const std::string &);
        protocol::network::packet::PacketHeader serializeClientIdentity(const std::string &id);
        std::shared_ptr<protocol::network::objectPacket> serializeObjectPacket(
                const std::shared_ptr<object::IObject> &gameObject);
    };
}

#endif //CPP_RTYPE_SERIALIZER_HPP
