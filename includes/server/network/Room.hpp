//
// Created by raidouane on 15/01/18.
//

#ifndef CPP_RTYPE_network_H
#define CPP_RTYPE_network_H

#include <vector>
#include <thread>

#include <bits/unique_ptr.h>
#include <queue>
#include "Client.hpp"
#include "server/engine/Engine.hpp"

namespace server::network {

    class Room {
    public:
        enum {
            MAX_CLIENTS = 4
        };
    private:
        std::vector<std::shared_ptr<server::network::Client>> _clients;
        std::shared_ptr<server::Engine>                       _engine;
        std::shared_ptr<game::IGame>                          &_game;
        bool                                                  _isRunning;
        std::thread                                           _roomThread;
        std::queue<std::string>                               _requestQ;
        std::queue<protocol::network::objectPacket>            _responseQ;

    public:
        Room(int id, std::shared_ptr<game::IGame> &);
        ~Room();

        std::vector<std::shared_ptr<server::network::Client>> getClients() const;
        std::shared_ptr<server::Engine> getEngine() const;
        bool addClient(std::shared_ptr<server::network::Client>);
        void removeClient(std::shared_ptr<server::network::Client>);
        void runWriter();
        void handleRoom();
        void run();
        void stop();
        void newClientEvent(std::string, protocol::network::packet::GameActionType);
    };
}

#endif //CPP_RTYPE_network_H
