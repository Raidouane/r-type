//
// Created by raidouane on 16/01/18.
//

#ifndef CPP_RTYPE_PLAYER_H
#define CPP_RTYPE_PLAYER_H

#include <string>
#include <vector>
#include <boost/asio/ip/udp.hpp>
#include "protocol/Protocol.hpp"

namespace server::network {

    class Room;

    class Client {
    private:
        std::string                            _id;
        std::string                            _ip;
        std::string                            _port;
        Room                                   *_room;
    public:
        boost::asio::ip::udp::socket           *_socket;
        boost::asio::ip::udp::endpoint         _remote_endpoint;

    public:
        Client(boost::asio::ip::udp::socket *,  boost::asio::ip::udp::endpoint *endpoint,
               const std::string& host, const std::string& port);
        ~Client();

        Room       *getRoom();

        std::string getId();
        void send(const boost::asio::mutable_buffers_1 &data);
        server::network::Room *getRoom() const;
        void setRoom(Room *);
    };
}


#endif //CPP_RTYPE_PLAYER_H
