#ifndef ROOM_MANAGER_HPP
#define ROOM_MANAGER_HPP

#include <vector>
#include <string>
#include <boost/asio/ip/udp.hpp>
#include "server/network/Room.hpp"

namespace server::core
{
    class RoomsManager
    {
    private:
        std::vector<std::shared_ptr<server::network::Client>>    _clients;
        std::vector<std::shared_ptr<server::network::Room>>      _rooms;
        std::shared_ptr<game::IGame>                    &_game;

    public:
        RoomsManager(std::shared_ptr<game::IGame> &);
        ~RoomsManager();

        void manageRooms(boost::asio::ip::udp::socket *, boost::asio::ip::udp::endpoint  *, char *data);
        std::shared_ptr<server::network::Client> getClient(const std::string &id) const;
        std::shared_ptr<server::network::Room> getIncompleteRoom();
        void findRoom(std::shared_ptr<server::network::Client>);
        void newClientHandle(boost::asio::ip::udp::socket *, boost::asio::ip::udp::endpoint  *);
        void removeClientHandle(boost::asio::ip::udp::endpoint  *);
        void eventsHandle(boost::asio::ip::udp::socket *, boost::asio::ip::udp::endpoint  *, protocol::network::packet::gameEvent *);
        void actionsHandle(boost::asio::ip::udp::socket *, boost::asio::ip::udp::endpoint  *, protocol::network::packet::gameAction *);
        void removeRoom(server::network::Room *);
        void removeClient(std::shared_ptr<server::network::Client>);
    };
}

#endif