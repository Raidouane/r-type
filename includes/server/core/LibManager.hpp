#ifndef LIB_MANAGER_HPP
#define LIB_MANAGER_HPP

#include "object/IObject.hpp"
#include "game/IGame.hpp"
#include <string>
#include <dlfcn.h>
#include <memory>

namespace server::core
{
    enum    libType
    {
        GAME,
        OBJECT
    };

    class LibManager
    {
        std::shared_ptr<game::IGame>        _gameLib;
        protocol::network::packet::GameObjectType                  _objType;
        std::shared_ptr<game::IGame>        (*_gameConstructor)();
        std::shared_ptr<object::IObject>    (*_objectConstructor)(std::string);
        protocol::network::packet::GameObjectType                  (*_objTypeGetter)();



    public:
        LibManager();
        ~LibManager();

        void loadLib(libType, std::string targetPath);

        std::shared_ptr<game::IGame>                          getGameLib();
        std::function<std::shared_ptr<object::IObject> (std::string)>    getObjConstructor();
        protocol::network::packet::GameObjectType                                    getObjType();

    private:
        void loadGameLib(std::string targetPath);
        void loadObjConstructor(std::string targetPath);
        void *openLib(libType, int, std::string targetPath);
    };
}

#endif