#ifndef DIR_MANAGER_HPP
#define DIR_MANAGER_HPP

#include <string>
#include <vector>
#include <dirent.h>
#include <sys/types.h>
#include <bits/unique_ptr.h>

namespace server::core
{
    class DirManager
    {
        std::vector<std::string>  _pathList;

    public:
        DirManager();
        ~DirManager();

        unsigned long            getNbFiles();
        std::vector<std::string>  getPathList() const;
        void                     generateFileList(std::string folderName);
        bool                     watchFolder(std::string folderName) const;
    };
}

#endif