#ifndef CORE_HPP
#define CORE_HPP

#include <queue>
#include <memory>
#include "RoomsManager.hpp"
#include "LibManager.hpp"
#include "DirManager.hpp"

namespace server
{
    class Core
    {
        std::unique_ptr<server::core::RoomsManager> _roomsManager;
        std::unique_ptr<server::core::LibManager>   _libManager;
        std::unique_ptr<server::core::DirManager>   _dirManager;
        std::shared_ptr<game::IGame>                _game;
        boost::asio::ip::udp::socket                _socket;
        boost::asio::ip::udp::endpoint              _remote_endpoint;

        enum {
            max_length = 2048
        };
        char                                _recv_buffer[max_length];

    public:
        Core(boost::asio::io_service &io_service);
        ~Core();

    private:
        void    loop();
        void    loadLibraries();
        void    loadConstructors();
        void    start_receive();
        void    handle_receive(const boost::system::error_code &error);
        void    handle_send(boost::shared_ptr <std::string> message,
                            const boost::system::error_code &error);
    };
}

#endif