//
// Created by raidouane on 16/01/18.
//

#ifndef CPP_RTYPE_ENGINE_H
#define CPP_RTYPE_ENGINE_H

#include <vector>
#include <queue>
#include "server/network/Client.hpp"
#include "server/network/Serializer.hpp"
#include "object/IObject.hpp"
#include "game/IGame.hpp"

namespace server {

    class Engine {
    private:
        std::shared_ptr<game::IGame>            const &_game;
        int                                    const _id;
        std::pair<std::size_t, std::size_t>    const _mapBounds;
        std::vector<std::shared_ptr<object::IObject>>    _objList;
        std::queue<protocol::network::objectPacket>        &_responseQ;

    public:
        Engine(int, std::shared_ptr<game::IGame>&, std::pair<std::size_t, std::size_t>, std::queue<protocol::network::objectPacket>&);
        ~Engine() {};

        void run();
        void addPlayer(std::string id);
        void eventObject(std::string id, protocol::network::packet::GameActionType type);
        void fillResponseQ();
        void removeObj(const std::string &id);
        void processObjectsEvent();
        int getId() const;

    private:
        void objectGestion();
        void moveObject(std::shared_ptr<object::IObject> &);
        void collisionCheck(std::shared_ptr<object::IObject> &);
        void objSetPos(std::shared_ptr<object::IObject> &, object::objectPos);
        bool hasCollision(std::size_t, std::size_t, std::size_t);
        bool checkObjPos(std::shared_ptr<object::IObject>&, std::shared_ptr<object::IObject>&);
    };
}

#endif //CPP_RTYPE_ENGINE_H
