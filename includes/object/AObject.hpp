#ifndef AOBJECT_HPP
#define AOBJECT_HPP

#include <memory>
#include <protocol/Packet.hpp>
#include <vector>
#include "object/IObject.hpp"

namespace object
{
    class AObject : public IObject
    {
    protected:
        std::string                             _assetPath;
        std::string                             _id;
        std::size_t                             _lp;
        std::shared_ptr<object::IObject>        _parentObject;
        objectParams                            _params;
        objectPos                               _pos;
        std::size_t                             _power;
        std::size_t                             _score;
        std::pair<std::size_t, std::size_t>     _size;
        protocol::network::packet::GameObjectType                              _type;
        protocol::network::packet::GameObjectType                              _subType;
        protocol::network::packet::GameObjectState                             _state;
        std::vector<protocol::network::packet::GameActionType>                 _actionList;
        unsigned long                                                        _indexActionList;

    public:
        AObject(std::string, protocol::network::packet::GameObjectType);
        AObject(std::string, protocol::network::packet::GameObjectType, std::shared_ptr<object::IObject>);
        virtual ~AObject() {};

        virtual std::string                        getId() const;
        virtual std::pair<std::size_t, std::size_t>  getSize() const;
        virtual objectPos                           getPos() const;
        virtual std::string                         getAssetPath() const;
        virtual objectParams                        getParams() const;
        virtual std::size_t                         getLp() const;
        virtual std::size_t                         getScore() const;
        virtual std::size_t                         getPower() const;
        virtual bool                                hasColl() const;
        virtual bool                                isAlive() const;
        virtual bool                                isAnimated() const;
        virtual std::shared_ptr<IObject>            getParentObj();
        virtual protocol::network::packet::GameObjectType                          getType() const;
        virtual protocol::network::packet::GameObjectType                          getSubType() const;
        virtual protocol::network::packet::GameActionType                          getNextAction();
        virtual protocol::network::packet::GameObjectState                         getState() const;

        virtual void                                setParentObj(const std::shared_ptr<IObject> &);
        virtual void                                setPos(const objectPos &);
        virtual void                                setSize(const std::pair<std::size_t, std::size_t> &);
        virtual void                                setAssetPath(const std::string &);
        virtual void                                setLp(const std::size_t &);
        virtual void                                setScore(const std::size_t &);
        virtual void                                setPower(const std::size_t &);
        virtual void                                setHasColl(const bool &);
        virtual void                                setOrientation(const objectOrientation &orientation);
        virtual void                                setSubType(const protocol::network::packet::GameObjectType &);
        virtual void                                setNextAction(const protocol::network::packet::GameActionType&);
        virtual void                                setState(const protocol::network::packet::GameObjectState &);
        virtual void                                nextAction();


    };

}

#endif