#ifndef BULLET_HPP
#define BULLET_HPP

#include "object/AObject.hpp"

namespace object
{
    class Bullet : public AObject
    {

        protocol::network::packet::GameActionType _nextAction;

    public:
        Bullet(std::string);
        virtual ~Bullet() {};

        virtual void  setSubType(const protocol::network::packet::GameObjectType &);
        virtual protocol::network::packet::GameActionType               getNextAction();
    };
}

#endif