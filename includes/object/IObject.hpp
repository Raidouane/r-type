#ifndef IOBJECT_HPP
#define IOBJECT_HPP

#include <utility>
#include <string>
#include <memory>
#include <protocol/Packet.hpp>

namespace object
{
    struct objectPos
    {
        std::size_t x;
        std::size_t y;
        std::size_t z;
    };

    enum class objectOrientation
    {
        NORTH,
        EST,
        SOUTH,
        WEST,
        UNDEFINED
    };

    struct  objectParams
    {
        bool    hasColl;
        bool    animated;
        std::size_t speed;
        objectOrientation orientation;
    };

    class IObject
    {
    public:
        IObject() {};
        virtual ~IObject() {};

        virtual std::string                          getId() const = 0;
        virtual std::pair<std::size_t, std::size_t>  getSize() const = 0;
        virtual objectPos                            getPos() const = 0;
        virtual std::string                          getAssetPath() const = 0;
        virtual objectParams                         getParams() const = 0;
        virtual std::size_t                          getLp() const = 0;
        virtual std::size_t                          getScore() const = 0;
        virtual std::size_t                          getPower() const = 0;
        virtual bool                                 hasColl() const = 0;
        virtual bool                                 isAlive() const = 0;
        virtual bool                                isAnimated() const = 0;
        virtual std::shared_ptr<IObject>             getParentObj() = 0;
        virtual protocol::network::packet::GameObjectType                          getType() const = 0;
        virtual protocol::network::packet::GameObjectType                          getSubType() const = 0;
        virtual protocol::network::packet::GameActionType                          getNextAction() = 0;
        virtual protocol::network::packet::GameObjectState                         getState() const = 0;


        virtual void                                setParentObj(const std::shared_ptr<IObject> &) = 0;
        virtual void                                setPos(const objectPos &) = 0;
        virtual void                                setSize(const std::pair<std::size_t, std::size_t> &) = 0;
        virtual void                                setAssetPath(const std::string &) = 0;
        virtual void                                setLp(const std::size_t &) = 0;
        virtual void                                setScore(const std::size_t &) = 0;
        virtual void                                setPower(const std::size_t &) = 0;
        virtual void                                setHasColl(const bool &) = 0;
        virtual void                                setOrientation(const objectOrientation &orientation) = 0;
        virtual void                                setSubType(const protocol::network::packet::GameObjectType &) = 0;
        virtual void                                setNextAction(const protocol::network::packet::GameActionType&) = 0;
        virtual void                                setState(const protocol::network::packet::GameObjectState &) = 0;
        virtual void                                nextAction() = 0;

    };
}

#endif