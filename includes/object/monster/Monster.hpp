#ifndef MONSTER_HPP
#define MONSTER_HPP

#include "object/AObject.hpp"

namespace object
{
    class Monster : public AObject
    {

        protocol::network::packet::GameActionType _nextAction;

    public:
        Monster(std::string);
        virtual ~Monster() {};

    private:
        void    initListActions();
    };
}

#endif