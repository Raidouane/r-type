#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "object/AObject.hpp"

namespace object
{
    class Player : public AObject
    {
        protocol::network::packet::GameActionType _nextAction;

    public:
        Player(std::string);
        virtual ~Player() {};

        virtual void                                                    nextAction();
        virtual protocol::network::packet::GameActionType               getNextAction();
        virtual void   setNextAction(const protocol::network::packet::GameActionType&);

    };
}

#endif